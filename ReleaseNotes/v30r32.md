   
!========================= DecFiles v30r32 2019-07-08 =======================    
    
! 2019-07-07 - Vanya BELYAEV (MR !314)  
   Add four new dec-files  for Lambda_b(5912)  and Lambda_b(5920) decays.  
   Tight cuts are coherent with those used for Lambda_b(6150) and Lambda_b(6080) dec-files  
   + 16146016 : Lbstar5912_Lbpipi,JpsipK=TightCut  
   + 16146017 : Lbstar5920_Lbpipi,JpsipK=TightCut  
   + 16166016 : Lbstar5912_Lbpipi,Lcpi=TightCut  
   + 16166017 : Lbstar5920_Lbpipi,Lcpi=TightCut  
  
! 2019-06-28 - Raul Rabadan (MR !307)  
  New decfile for B2DstDspi Amplitude Analysis  
  + 11196091 : Bd_DstDs,D0pi,KKpi=DDALITZ,DecProdCut,WithMinP.dec   
  
! 2019-07-05 - Dmitrii Pereima (MR !313)  
   Add new decay file:  
   + 12245002  Bu_psi3823K,Jpsipipi=TightCut.dec  
  
! 2019-07-05 - Michal Kreps (MR !312)  
   Remove double definition of Sigma(1775)0 and its antiparticle from DECAY.DEC. Second definition is kept, which should be one used up to now.  
  
! 2019-07-05 - Michal Kreps (MR !311)  
   Fix creation of list of cuts in decparser  
  
! 2019-07-02 - Marco Pappagallo (MR !310)  
   Added 7 decfiles for X(3872) -> Jpsi mu mu  
   + 18144011 : incl_b=X3872,Jpsimumu,DecProdCut,PPChange.dec  
   + 18144012 : incl_b=X3872,Jpsipipi,DecProdCut,PPChange.dec  
   + 18146011 : incl_b=X3872,Jpsipipi,munumunu,DecProdCut,PPChange.dec  
   + 18144041 : incl_b=chic1,Jpsimumu,DecProdCut.dec  
   + 18146001 : incl_b=psi2S,Jpsipipi,munumunu,DecProdCut,PPChange.dec  
   + 12145009 : Bu_X3872K,Jpsimumu=DecProdCut.dec  
   + 11146001 : Bd_X3872Kpi,Jpsimumu=DecProdCut.dec  
  
! 2019-07-01 - Marco Pappagallo (MR !309)  
   + 16165234 : Omegab_XicprimeKpi,pKpi=PPChange,DecProdCut.dec  
   + 16165131 : Omegab_XicKpi,pKpi=PPChange,TightCut,mXicK3300MeV.dec  
  
! 2019-06-26 - Mariia Poliakova (MR !306)  
   Add new decay file  
   + 17574081 : Bs2st_BuK,D0munu,Kpipi,mm=DecProdCut.dec  
  
! 2019-07-01 - Marco Pappagallo (MR !308)  
   Add 3 decfiles for Xicst* -> Lc K analysis  
   + 26165092 : Xic3055+_SigmacK,Lcpi,pKpi=phsp,TightCut.dec   
   + 26165094 : Xic3080+_Sigmac2520K,Lcpi,pKpi=phsp,TightCut.dec  
   + 26165093 : Xic3080+_SigmacK,Lcpi,pKpi=phsp,TightCut.dec  
  
  
! 2019-06-03 - Harris Bernstein (MR !295)  
   New decay files  
   + 11299015 : Bd_DDKst0,3pi=cocktail,TightCut.dec  
   + 13499415 : Bs_DsDKst0,3pi=cocktail,TightCut.dec  
   + 12299015 : Bu_D0DKst0,3pi=cocktail,TightCut.dec  
   + 15199115 : Lb_LcDKst0,3pi=cocktail,TightCut.dec  
  
