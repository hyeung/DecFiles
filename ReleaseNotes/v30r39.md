
!========================= DecFiles v30r39 2019-11-13 =======================  
  
! 2019-11-09 - Leon David Carus (MR !375)  
   Add new decay file  
   + 12113026 : Bu_MuMajoranaNeutrino2MuPi,m=4500MeV,t=10ps,DecProdCut  
  
! 2019-11-08 - Tommaso Pajero (MR !374)  
   Add 2 new decay files  
   + 11774004 : Bd_DstX,cocktail,D0pi,Kpi=TightCut  
   + 12365401 : Bu_DstX,cocktail,D0pi,Kpi=TightCut  
  
! 2019-11-05 - Titus Mombacher (MR !373)  
   Add new decay file  
   + 14543043 : Bc_Jpsimunu,mm=BcVegPy,ffEbert,DiLeptonInAcc,M4.5GeV  
  
! 2019-11-01 - Yixiong Zhou (MR !372)  
   Add 8 new decay files  
   + 12145063 : Bu_X4140,Jpsiphi=DecProdCut,InAcc  
   + 12145064 : Bu_X4274,Jpsiphi=DecProdCut,InAcc  
   + 12145065 : Bu_X4500,Jpsiphi=DecProdCut,InAcc  
   + 12145066 : Bu_X4700,Jpsiphi=DecProdCut,InAcc  
   + 28144006 : X4140,Jpsiphi=DecProdCut,InAcc  
   + 28144007 : X4274,Jpsiphi=DecProdCut,InAcc  
   + 28144008 : X4500,Jpsiphi=DecProdCut,InAcc  
   + 28144009 : X4700,Jpsiphi=DecProdCut,InAcc  
  
! 2019-10-31 - Vitalii Lisovskyi (MR !371)  
   Add new decay file  
   + 16115133 : Xib_Ximumu,Lambdapi=phsp,DecProdCut  
  
! 2019-10-31 - Biplab Dey (MR !370)  
   Add 2 new decay files  
   + 11104563 : Bd_Kspi+pi-eta=TightCut,mKshhCut,PHSP  
   + 13104513 : Bs_KsK+pi-eta=TightCut,mKshhCut,PHSP  
  
! 2019-10-30 - Chenxi Gu (MR !368)  
   Add new decay file  
   + 23103023 : Ds+_K-K+pi+=res,DecProdCut,PTYcut  
  
! 2019-10-25 - Tommaso Pajero (MR !367)  
   Add 3 new decay files  
   + 27163101 : Dst_D0pi,KK=TightCut,2  
   + 27163100 : Dst_D0pi,Kpi=TightCut,2  
   + 27163102 : Dst_D0pi,pipi=TightCut,2  
  
