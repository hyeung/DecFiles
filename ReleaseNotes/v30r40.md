!========================= DecFiles v30r40 2019-11-30 =======================  
  
! 2019-11-29 - Michal Kreps (MR !386)  
   Modify scripts to create options and decparser to work with both python2 and python3. Options files generated after modifications are identical to the ones before changes and Gauss was tested to run fine with one of the new options file. There are few quirks which we might want to remove in future when we do not have to support python2 anymore.  
  
! 2019-11-29 - Mick Mulder (MR !385)  
   Add 2 new decay files  
   + 15124102 : Lb_Lambdaee=LQCD,DecProdCut  
   + 15114102 : Lb_Lambdamumu=LQCD,DecProdCut  
  
! 2019-11-28 - Cristina Sanchez Gras (MR !384)  
   Add 2 new decay files  
   + 49152001 : cep_psi2S_ee  
   + 49142001 : cep_psi2S_mumu  
  
! 2019-11-27 - Carmen Giugliano (MR !383)  
   Add new decay file  
   + 13563000 : Bs_DsTauNu,KKPi,PiPiPi=DecProdCut,tauola  
  
! 2019-11-26 - Adam Morris (MR !382)  
   Add new decay file  
   + 11494700 : Bd_DstDsX,Ds2Xa1=DecProdCut  
  
! 2019-11-22 - Vitalii Lisovskyi (MR !381)  
   Add 3 new decay files  
   + 16145135 : Xib_JpsiXi,mm,Lambdapi=TightCut  
   + 16115135 : Xib_Ximumu,Lambdapi=phsp,TightCut  
   + 16145137 : Xib_psi2SXi,mm,Lambdapi=TightCut  
  
! 2019-11-21 - Philipp Ibis (MR !380)  
   Add 7 new decay files  
   + 11196004 : Bd_D+D-,KKpi=CPV,DDALITZ,DecProdCut,pCut1600MeV  
   + 11196002 : Bd_D+D-,Kpipi,KKpi=CPV,DDALITZ,DecProdCut,pCut1600MeV  
   + 11196001 : Bd_D+D-,Kpipi=CPV,DDALITZ,DecProdCut,pCut1600MeV  
   + 11196006 : Bd_Ds+D-,piKpi,KKpi=DDALITZ,DecProdCut,pCut1600MeV  
   + 11196005 : Bd_Ds+D-,piKpi,Kpipi=DDALITZ,DecProdCut,pCut1600MeV  
   + 11196008 : Bd_Ds+D-,pipipi,KKpi=DDALITZ,DecProdCut,pCut1600MeV  
   + 11196007 : Bd_Ds+D-,pipipi,Kpipi=DDALITZ,DecProdCut,pCut1600MeV  
  
! 2019-11-20 - Guy Henri Maurice Wormser (MR !379)  
   Modify decay files (two with new event type)  
   + 11896612: Bd_DstXc,Xc2hhhNneutrals,upto5prongs=DecProdCut  
   + 11996413: Bd_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut  
   + 12997613: B+_excitedDstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut  
   + 13996612: Bs_DstXc,Xc2hhhNneutrals_cocktail,upto5prongs=DecProdCut  
  
! 2019-11-19 - Daniel Charles Craik (MR !378)  
   Add 26 new decay files  
   + 11104471 : Bd_KstarDarkBoson2Etapipi,gg,m=1000MeV,t=100ps,DecProdCut  
   + 11104461 : Bd_KstarDarkBoson2Etapipi,gg,m=1000MeV,t=1ps,DecProdCut  
   + 11104472 : Bd_KstarDarkBoson2Etapipi,gg,m=1200MeV,t=100ps,DecProdCut  
   + 11104462 : Bd_KstarDarkBoson2Etapipi,gg,m=1200MeV,t=1ps,DecProdCut  
   + 11104473 : Bd_KstarDarkBoson2Etapipi,gg,m=1500MeV,t=100ps,DecProdCut  
   + 11104463 : Bd_KstarDarkBoson2Etapipi,gg,m=1500MeV,t=1ps,DecProdCut  
   + 11104474 : Bd_KstarDarkBoson2Etapipi,gg,m=2000MeV,t=100ps,DecProdCut  
   + 11104464 : Bd_KstarDarkBoson2Etapipi,gg,m=2000MeV,t=1ps,DecProdCut  
   + 11104475 : Bd_KstarDarkBoson2Etapipi,gg,m=3000MeV,t=100ps,DecProdCut  
   + 11104465 : Bd_KstarDarkBoson2Etapipi,gg,m=3000MeV,t=1ps,DecProdCut  
   + 11104476 : Bd_KstarDarkBoson2Etapipi,gg,m=4000MeV,t=100ps,DecProdCut  
   + 11104466 : Bd_KstarDarkBoson2Etapipi,gg,m=4000MeV,t=1ps,DecProdCut  
   + 11104460 : Bd_KstarEtapr,Etapipi,gg,DecProdCut  
   + 12103471 : Bu_KDarkBoson2Etapipi,gg,m=1000MeV,t=100ps,DecProdCut  
   + 12103461 : Bu_KDarkBoson2Etapipi,gg,m=1000MeV,t=1ps,DecProdCut  
   + 12103472 : Bu_KDarkBoson2Etapipi,gg,m=1200MeV,t=100ps,DecProdCut  
   + 12103462 : Bu_KDarkBoson2Etapipi,gg,m=1200MeV,t=1ps,DecProdCut  
   + 12103473 : Bu_KDarkBoson2Etapipi,gg,m=1500MeV,t=100ps,DecProdCut  
   + 12103463 : Bu_KDarkBoson2Etapipi,gg,m=1500MeV,t=1ps,DecProdCut  
   + 12103474 : Bu_KDarkBoson2Etapipi,gg,m=2000MeV,t=100ps,DecProdCut  
   + 12103464 : Bu_KDarkBoson2Etapipi,gg,m=2000MeV,t=1ps,DecProdCut  
   + 12103475 : Bu_KDarkBoson2Etapipi,gg,m=3000MeV,t=100ps,DecProdCut  
   + 12103465 : Bu_KDarkBoson2Etapipi,gg,m=3000MeV,t=1ps,DecProdCut  
   + 12103476 : Bu_KDarkBoson2Etapipi,gg,m=4000MeV,t=100ps,DecProdCut  
   + 12103466 : Bu_KDarkBoson2Etapipi,gg,m=4000MeV,t=1ps,DecProdCut  
   + 12103469 : Bu_KEtapr,Etapipi,gg,DecProdCut  
  
! 2019-11-14 - Alison Maria Tully (MR !377)  
   Add new decay file  
   + 10005000 : incl_b=ChargedTracksCut  
  
! 2019-11-13 - Maria Vieites Diaz (MR !376)  
   Add new decay file  
   + 12203402 : Bu_K1pi0,Kpipi=mK1270,gg,DecProdCut  
  
! 2019-09-30 - Mark Smith (MR !358)  
   Add 3 new decay files  
   + 11874110 : Bd_LcpX,pmuX=TightCut  
   + 12875003 : Bu_Dstpimunu,Kpi=cocktail  
   + 15876401 : Lb_Lcppbarmunu,pX=TightCut  
  
