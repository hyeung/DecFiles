!========================= DecFiles v30r48 2020-07-29 =======================  
  
! 2020-07-29 - Michal Kreps (MR !559)  
   Add Ds+ to list of particles for which signal particle gun is possible.  
  
! 2020-07-28 - Ryan Lawrence Newcombe (MR !558)  
   Add 2 new decay files  
   + 12573004 : Bu_Dstppmunu=TightCut  
   + 12573002 : Bu_Dststppmunu=TightCut  
  
! 2020-07-27 - Benedetto Gianluca Siddi (MR !557)  
   Fix decay files  
   + 21263005 : incl_b=D+,Kpipi,3pi=DDALITZ,DecProdCut,ExtraParticles  
   + 23903000 : incl_b=Ds,KKpi,3pi=DDALITZ,DecProdCut,ExtraParticles   
  
! 2020-07-24 - Svende Annelies Braun (MR !555)  
   Add 6 new decay files  
   + 11574021 : Bd_Dst+munu,D0pi+=RDstar,TightCut  
   + 11574011 : Bd_Dst+taunu,D0pi+,mununu=RDstar,TightCut  
   + 12573012 : Bu_D0munu=RDstar,TightCut  
   + 12573001 : Bu_D0taunu,mununu=RDstar,TightCut  
   + 12773410 : Bu_Dst0munu,D0pi0=RDstar,TightCut  
   + 12773400 : Bu_Dst0taunu,D0pi0,mununu=RDstar,TightCut  
  
! 2020-07-21 - Donal Hill (MR !554)  
   Add new decay file  
   + 11196230 : Bd_Ds2460Dst,Dsgamma,D0pi=DDALITZ,DecProdCut  
  
! 2020-07-15 - Zhihong Shen (MR !553)  
   Add new decay file  
   + 12145121 : Bu_psi2SKS0pi,mm=DecProdCut  
  
! 2020-07-13 - Steven R. Blusk (MR !551)  
   Add 2 new decay files  
   + 16267031 : Xib_Lb3pi,pKpi=TightCut  
   + 16365031 : Xib_Lbpi,pKpi=TightCut  
  
! 2020-07-09 - Michal Kreps (MR !550)  
   Move regex flags to start of the expression to avoid deprecation warning with python 3.  
  
! 2020-07-08 - Louis Lenard Gerken (MR !549)  
   Fix decay file  
   + 11196002 : Bd_D+D-,Kpipi,KKpi=CPV,DDALITZ,DecProdCut,pCut1600MeV  
  
