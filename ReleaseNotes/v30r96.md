DecFiles v30r96 2023-08-21 
==========================  
 
! 2023-08-18 - Jialu Wang (MR !1608)  
   Modify decay file  
   + 11104145 : Bd_Lambdappi=TightCut  
  
! 2023-08-12 - Zeqing Mu (MR !1607)  
   Add new decay file  
   + 14165092 : Bc_DstKpi,D0pi,Kpi=BcVegPy,DecProdCut  
  
! 2023-08-07 - Harry Victor Cliff (MR !1600)  
   Add 4 new decay files  
   + 12583020 : Bu_D0enu,Kenu=DecProdCut,ptCut200MeV,visMass4GeV  
   + 12583030 : Bu_D0enu,pienu=DecProdCut,ptCut200MeV,visMass4GeV  
   + 12583040 : Bu_D0pi,Kenu=DecProdCut,ptCut200MeV  
   + 12583050 : Bu_D0pi,pienu=DecProdCut,ptCut200MeV  
  
! 2023-08-03 - Yunxuan Song (MR !1599)  
   Modify 2 decay files  
   + 15826000 : Lb_pKtautau,3pipi0e=DecProdCut,tauolababar,phsp  
   + 15816000 : Lb_pKtautau,3pipi0mu=DecProdCut,tauolababar,phsp  
  
! 2023-07-31 - Ziyu Bai (MR !1589)  
   Add 2 new decay files  
   + 36103101 : Omega_LambdaK-=DecProdCut  
   + 36103103 : Omega_Lambdapi-=DecProdCut  
  
! 2023-07-14 - Patrick Haworth Owen (MR !1579)  
   Modify 2 decay files  
   + 11694605 : Bd_DD,DD=cocktail,D+muRDplusCutFixed  
   + 12695405 : Bu_DD,DD=cocktail,D+muRDplusCutFixed  
  
