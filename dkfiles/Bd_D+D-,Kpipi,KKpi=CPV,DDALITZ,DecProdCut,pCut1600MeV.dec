# EventType: 11196002
#
# Descriptor: [B0-> (D+ -> pi- K+ K+) (D- -> K+ pi- pi-)]cc
#
# NickName: Bd_D+D-,Kpipi,KKpi=CPV,DDALITZ,DecProdCut,pCut1600MeV
#
# Cuts: DaughtersInLHCbAndWithMinP
#
# ExtraOptions: TracksInAccWithMinP
#
# Documentation:  B0->D+D- with CP violation and D decaying by D_Dalitz
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Philipp Ibis
# Email: philipp.ibis@cern.ch
# Date: 20191118
#
# P(1) = deltaM = 0.5065e12          (HFLAV for PDG 2019)
# P(2) = dg/g = 0
# P(3) = |q/p| = 1
# P(4) = arg(q/p) = -0.997          (corresponds to sin(2beta)=0.84 in B0->DD, HFLAV Summer 2018)
# P(5) = |Af| = 1.0
# P(6) = arg(Af) = 0.
# P(7) = |Abarf| = 1.0
# P(8) = arg(Abarf) = 0.


# --------------------
# DEFINE THE D+ AND D-
# --------------------
Alias      MyD+  D+
Alias      MyD-  D-
ChargeConj MyD+  MyD-


# ---------------------------------------
# DEFINE THE D+ AND D- for the KKpi decay
# ---------------------------------------
Alias      MyDother+     D+
Alias      MyDother-     D-
ChargeConj MyDother+     MyDother-


# ---------------
# DECAY OF THE B0
# ---------------
Decay B0sig
  0.500     MyD+       MyDother-             SSD_CP 0.5065e12 0.0 1.0 -0.997 1.0 0.0 1.0 0.0;
  0.500     MyDother+   MyD-                 SSD_CP 0.5065e12 0.0 1.0 -0.997 1.0 0.0 1.0 0.0;
Enddecay
CDecay anti-B0sig


# ---------------
# DECAY OF THE D-
# ---------------
Decay MyD-
  1.000  K+  pi- pi-                    D_DALITZ;
Enddecay
CDecay MyD+


# -----------------------
# DECAY OF THE D- to KKpi
# -----------------------
Decay MyDother-
  1.000  K-  K- pi+                    D_DALITZ;
Enddecay
CDecay MyDother+


End

