# EventType: 11198061
#
# Descriptor: [B0 => (D_s+ ==> pi+ pi- pi+) ( D*(2010)- => (anti-D0 ==> K+ pi- pi+ pi-) pi- ) ]cc
#
# NickName: Bd_Dst-Ds+,D0pi,Kpipipi,pipipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[ [B~0]cc --> (D0 ==> K- pi+ pi- pi+) ... ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#     ,'inAcc = ( 0 < GPZ )  &  ( 200 * MeV < GPT ) & ( 900 * MeV < GP ) & in_range ( 1.8 , GETA , 5.2 )'
#     ,"nPiB = GNINTREE(('pi+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"nKB = GNINTREE(('K+' == GABSID) & inAcc, HepMC.descendants)"
#     ,"npB = GNINTREE(('p+' == GABSID) & inAcc , HepMC.descendants)"
#     ,"nMuB = GNINTREE(('mu+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"neB = GNINTREE(('e+' == GABSID) & inAcc & ( GNINTREE ( ('KS0' == GABSID) | ('KL0' == GABSID), HepMC.parents)==0 ), HepMC.descendants)"
#     ,"goodD0 = GINTREE(( 'D0'  == GABSID ) & (GP>19000*MeV) & (GPT>1900*MeV) & ( GNINTREE(( 'K-' == GABSID ) & ( GPT > 350*MeV ) & ( GP > 3900*MeV ) & inAcc, HepMC.descendants) == 1 ) & ( GNINTREE(( 'pi+' == GABSID ) & ( GPT > 200*MeV ) & ( GP > 1900*MeV ) & inAcc, HepMC.descendants) == 3 ))"
#     ,"goodB = ( goodD0 & (nPiB+nKB+nMuB+neB+npB >= 7) )"
# ]
# tightCut.Cuts = {
#     '[B~0]cc': 'goodB'
#     }
# EndInsertPythonCode
#
# Documentation: B0 -> ( D*- (D0 -> Kpipipi) pi) (Ds -> 3pi). Ds->3pi with D_DALITZ.
# Cuts from *B2XTauNuAllLines*.
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# CPUTime: < 1 min
# Responsible: A. Romero Vidal
# Email: antonio.romero@usc.es
# Date:   20221130
#
Alias      MyD*-       D*-
Alias      MyD*+       D*+
ChargeConj MyD*+       MyD*-
#
Alias      MyD_s+      D_s+
Alias      MyD_s-      D_s-
ChargeConj MyD_s+      MyD_s-
#
Alias      MyD0        D0
Alias      Myanti-D0   anti-D0
ChargeConj MyD0        Myanti-D0
#
Decay B0sig
1.000      MyD*-       MyD_s+     SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyD_s+
  1.000     pi-        pi+        pi+        D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD*-
  1.000        Myanti-D0 pi-                    VSS;
Enddecay
CDecay MyD*+
#
Decay MyD0
1.00      K-           pi+        pi+        pi-      LbAmpGen DtoKpipipi_v2; # (0.0822 +- 0.0014) incl.;
Enddecay
CDecay Myanti-D0
#
End

