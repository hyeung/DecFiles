# EventType: 11166142
#
# Descriptor: [B0 -> (D*- -> (anti-D0 -> (K_S0 -> pi+ pi-) pi+ pi-) pi-) pi+]cc
#
# NickName: Bd_Dst-pi+,D0pi,KSpipi=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
#InsertPythonCode:
##
#from Configurables import LoKi__GenCutTool
#from Gauss.Configuration import *
#Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool,'TightCut')
#tightCut = Generation().SignalRepeatedHadronization.TightCut
#tightCut.Decay = '^[B0 => ^(D*(2010)- -> ^(D~0 => ^(KS0 => pi+ pi-) pi+ pi-) pi- ) pi+]CC'
#tightCut.Preambulo += [
#    'GVZ = LoKi.GenVertices.PositionZ()',
#    'from GaudiKernel.SystemOfUnits import millimeter',
#    'inAcc        = (in_range(0.010, GTHETA, 0.400))',
#    'goodB       = (GP > 25000 * MeV) & (GPT > 1500 * MeV) & (GTIME > 0.050 * millimeter)',
#    'goodDstarm       = (GP >  20000 * MeV) & (GPT > 2500 * MeV)',
#    'goodD0       = (GP >  10000 * MeV) & (GPT > 500 * MeV)',
#    'goodD0Pi = (GNINTREE( ("pi+"==GABSID) & (GP > 1000 * MeV) & inAcc, 1) > 1.5)',
#    'goodKSPi  = (GNINTREE( ("pi+"==GABSID) & (GP >  1750 * MeV) & inAcc, 1) > 1.5)',
#    'goodKS       = (GP >  3500 * MeV) & (GPT > 250 * MeV) ',
#    'goodBPi = (GNINTREE( ("pi+"==GABSID) & (GP > 4000 * MeV) & (GPT > 400 * MeV) & inAcc, 1) > 0.5)',
#]
#tightCut.Cuts = {
#    '[B0]cc'          : 'goodB & goodBPi',
#    '[D*(2010)-]cc'   : 'goodDstarm',
#    '[D0]cc'          : 'goodD0 & goodD0Pi',
#    '[KS0]cc'         : 'goodKS & goodKSPi',
#    }
#EndInsertPythonCode
#
# Documentation: D*+ forced into D0 pi+, D0 forced to KSpipi
# EndDocumentation
#
# PhysicsWG: B2OC
# CPUTime: <1 min
# Tested: Yes
# Responsible: Victor Daussy-Renaudin
# Email: victor.renaudin@cern.ch
# Date: 20190808
#
Alias             MyD*-       D*-
Alias             Myanti-D0   anti-D0
Alias             MyD*+       D*+
Alias             MyD0        D0
Alias             MyK_S0      K_S0
ChargeConj        MyD*+       MyD*-
ChargeConj        MyD0        Myanti-D0
ChargeConj        MyK_S0      MyK_S0
#
Decay B0sig
1.000        MyD*-     pi+                    SVS;
Enddecay
CDecay anti-B0sig
#
Decay MyD*-
1.000        Myanti-D0 pi-                    VSS;
Enddecay
CDecay MyD*+
#
Decay Myanti-D0
  1.000     MyK_S0 pi+  pi-        PHSP;
Enddecay
CDecay MyD0
#
Decay MyK_S0
  1.000     pi+  pi-                      PHSP;
Enddecay
#
End

