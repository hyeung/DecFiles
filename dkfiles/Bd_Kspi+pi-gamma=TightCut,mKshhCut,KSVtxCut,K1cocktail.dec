# EventType: 11204302 
#
# Descriptor: [B0 -> (K_10 -> (KS0 -> pi+ pi-) (rho0 -> pi+ pi-)) gamma]cc
#
# NickName: Bd_Kspi+pi-gamma=TightCut,mKshhCut,KSVtxCut,K1cocktail
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 1 min
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation()
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# evtgendecay = ToolSvc().EvtGenDecayWithCutTool
# evtgendecay.CutTool = "LoKi::GenCutTool/mKshhCut"
# evtgendecay.addTool(LoKi__GenCutTool ,'mKshhCut')
# evtgendecay.mKshhCut.Decay = "[Beauty ==>  gamma pi+ pi- KS0]CC"
# evtgendecay.mKshhCut.Cuts  = {'[B0]cc' : ' mKshhCut '}
# evtgendecay.mKshhCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "CS         = LoKi.GenChild.Selector",
#     "mKshhCut   = ( GMASS( CS('[Beauty ==>  gamma ^pi+ pi- KS0 ]CC'), CS('[Beauty ==>  gamma pi+ ^pi- KS0]CC'), CS('[Beauty ==>  gamma pi+ pi- ^KS0]CC')) < 2.0 * GeV )"
#     ]
#
# gen.SignalRepeatedHadronization.setProp('MaxNumberOfRepetitions', 5000)
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[B0 ==>  gamma ^pi+ ^pi- ^(KS0 => ^pi+ ^pi-)]CC' # this photon ``might'' not be a direct dau of B due to intermediate res
# tightCut.Cuts      =    {
#     '[pi+]cc'        : 'inAcc' , 
#     '[B0]cc'         : 'GINTREE(goodPhoton)' , 
#     'KS0'            : 'decayBeforeTT'
#     }
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inEcalX       =   abs ( GPX / GPZ ) < 4.5  / 12.5",
#     "inEcalY       =   abs ( GPY / GPZ ) < 3.5  / 12.5",
#     "inEcalHole    = ( abs ( GPX / GPZ ) < 0.25 / 12.5 ) & ( abs ( GPY / GPZ ) < 0.25 / 12.5 )",
#     "InEcal        = inEcalX & inEcalY & ~inEcalHole ",
#     "inAcc         = in_range ( 0.005 , GTHETA , 0.400 ) " , 
#     "goodPhoton    = ('gamma' == GABSID) & ( GPT > 1.8 * GeV ) & InEcal", 
#     "decayBeforeTT = GVEV & ( GFAEVX ( GVZ , 1.e+10 ) < 2400 * millimeter)"
#    ]
#
# EndInsertPythonCode
#
# Documentation: for TD-CPV KsPiPig, cocktail of PHSP + K1(1270)-> {rho0 Ks, K*pi}, gamma PT > 1.8 GeV, m(Kspipi) < 2.0 GeV and inAcceptance, KS0 VTZ < 2.4 m
# EndDocumentation
#
# PhysicsWG: RD
# Tested: Yes
# Responsible: Biplab Dey
# Email:  biplab.dey@.cern.ch
# Date: 20201119
#
Alias      MyK_10       K_10
Alias      Myanti-K_10  anti-K_10
ChargeConj MyK_10       Myanti-K_10
#
Alias      MyK*-      K*-
Alias      MyK*+      K*+
ChargeConj MyK*+      MyK*-
#
Alias      MyK0s  K_S0
ChargeConj MyK0s  MyK0s
#
Alias      Myrho0     rho0
ChargeConj Myrho0     Myrho0
#
LSNONRELBW Myrho0
BlattWeisskopf Myrho0 0.0
Particle Myrho0 0.775 0.15
ChangeMassMin Myrho0 0.35
ChangeMassMax Myrho0 2.0
#
LSNONRELBW MyK_10
BlattWeisskopf MyK_10 0.0
Particle MyK_10 1.272 0.2
ChangeMassMin MyK_10 0.9
ChangeMassMax MyK_10 2.5
#
LSNONRELBW MyK*-
BlattWeisskopf MyK*- 0.0
Particle MyK*- 0.892 0.06
ChangeMassMin MyK*- 0.7
ChangeMassMax MyK*- 3.0
#
LSNONRELBW Myanti-K_10
BlattWeisskopf Myanti-K_10 0.0
Particle Myanti-K_10 1.272 0.2
ChangeMassMin Myanti-K_10 0.9
ChangeMassMax Myanti-K_10 2.5
#
LSNONRELBW MyK*+
BlattWeisskopf MyK*+ 0.0
Particle MyK*+ 0.892 0.06
ChangeMassMin MyK*+ 0.7
ChangeMassMax MyK*+ 3.0
#
Decay B0sig
 0.96   gamma MyK0s pi+ pi- PHSP;
 0.04   gamma MyK_10   PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyK_10
  0.5   MyK*+    pi-       PHSP;
  0.5   MyK0s    Myrho0       PHSP;
Enddecay
CDecay Myanti-K_10
#
Decay MyK*+
  1.0   MyK0s    pi+       PHSP;
Enddecay
CDecay MyK*-
#
Decay MyK0s
  1.0   pi+      pi-       PHSP;
Enddecay
#
Decay Myrho0
  1.0   pi+      pi-       PHSP;
Enddecay
#
End
