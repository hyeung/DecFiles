# EventType: 11494601
#
# Descriptor: {[B0 -> (D*- -> pi- (anti-D0 -> K+ pi-)) (D_s+ -> (a_1+ -> rho0 pi+) ...) ... ]cc}
# NickName: Bd_excitedDstDsX,Ds2Xa1=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# gen = Generation().SignalRepeatedHadronization
# gen.addTool( LoKi__GenCutTool, "TightCut" )
# tightCut = gen.TightCut
# tightCut.Decay = "[(Beauty & LongLived) --> ^(D*(2010)+ => ^(D0 => K- pi+) pi+) pi- pi+ pi- ...]CC"
# tightCut.Cuts = {
#     "(Beauty & LongLived)" : "good_B",
#     "[D*(2010)+]cc" : "good_Dstar",
#     "[D0]cc" : "good_D0",
# }
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV, GeV",
#     "inAcc = ( GPZ > 0 ) & ( GPT > 40 * MeV ) & ( GP > 1.6 * GeV ) & in_range ( 1.8, GETA, 5.0 ) & in_range ( 0.005, GTHETA, 0.400 )",
#     "nPi = GCOUNT(('pi+' == GABSID) & inAcc, HepMC.descendants)",
#     "nK  = GCOUNT(('K-'  == GABSID) & inAcc, HepMC.descendants)",
#     "good_slow_pion = ('pi+' == GABSID) & (GPT >  40 * MeV) & inAcc",
#     "good_D0_pion   = ('pi+' == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0_kaon   = ('K+'  == GABSID) & (GPT > 140 * MeV) & inAcc",
#     "good_D0        = ('D0'  == GABSID) & (GPT > 1.5 * GeV) & ( GP > 15. * GeV ) & GCHILDCUT(good_D0_kaon, 'Charm => ^(K+|K-) (pi+|pi-)') & GCHILDCUT(good_D0_pion, 'Charm => (K+|K-) ^(pi+|pi-)')",
#     "good_Dstar     = ('D*(2010)+' == GABSID) & GCHILDCUT(good_slow_pion, 'Charm => Charm ^(pi+|pi-)')",
#     "good_B         = (nK >= 1) & (nPi >= 5)",
# ]
# EndInsertPythonCode

# Documentation: B0 -> D**(-> D* pi) Ds X cocktail, where Ds decays to final states with at least 3 charged pions and may come from D_s*, D_s*(2317), D_s(2457) or D_s(2536) and the D* must come from D_1, D_1' or D_2*.
# Background for B2XTauNu analyses. Missing modes from previous B->D*DsX cocktails.
# EndDocumentation
#
# CPUTime: <1 min 
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Adam Morris
# Email: adam.morris@cern.ch
# Date: 20191213
#
Alias           Myf0            f_0
ChargeConj      Myf0            Myf0
#
Alias           Mya1+           a_1+
Alias           Mya1-           a_1-
ChargeConj      Mya1+           Mya1-
#
Alias           MyD_s+          D_s+
Alias           MyD_s-          D_s-
ChargeConj      MyD_s+          MyD_s-
#
Alias           MyD_s*(2317)+   D_s0*+
Alias           MyD_s*(2317)-   D_s0*-
ChargeConj      MyD_s*(2317)+   MyD_s*(2317)-
#
Alias           MyD_s*+         D_s*+
Alias           MyD_s*-         D_s*-
ChargeConj      MyD_s*+         MyD_s*-
#
#
Alias           MyD_s*(2457)+   D_s1+
Alias           MyD_s*(2457)-   D_s1-
ChargeConj      MyD_s*(2457)+   MyD_s*(2457)-
#
Alias           MyD_s*(2536)+   D'_s1+
Alias           MyD_s*(2536)-   D'_s1-
ChargeConj      MyD_s*(2536)+   MyD_s*(2536)-
#
Alias           MyMainD*+       D*+
Alias           MyMainD*-       D*-
ChargeConj      MyMainD*+       MyMainD*-
#
Alias           MyD0            D0
Alias           anti-MyD0       anti-D0
ChargeConj      MyD0            anti-MyD0
#
Alias           MyD'_1-         D'_1-
Alias           MyD'_1+         D'_1+
ChargeConj      MyD'_1-         MyD'_1+
#
Alias           MyD_1-          D_1-
Alias           MyD_1+          D_1+
ChargeConj      MyD_1-          MyD_1+
#
Alias           MyD_2*-         D_2*-
Alias           MyD_2*+         D_2*+
ChargeConj      MyD_2*-         MyD_2*+
#
#
Decay B0sig

####### D* excite et Ds excited  de B0
 0.00053        MyD'_1-         MyD_s+          SVS;
 0.0008         MyD'_1-         MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00012        MyD'_1-         MyD_s*(2317)+   SVS;
 0.0007         MyD'_1-         MyD_s*(2457)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.000024       MyD'_1-         MyD_s*(2536)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
#
 0.0016         MyD_1-          MyD_s+          SVS;
 0.002          MyD_1-          MyD_s*+         SVV_HELAMP 0.48 0.0 0.734 0.0 0.48 0.0;
 0.00024        MyD_1-          MyD_s*(2317)+   SVS;
 0.0013         MyD_1-          MyD_s*(2457)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.00005        MyD_1-          MyD_s*(2536)+   SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
# below divided by 3 compared to B+ -> D2*0 Ds X to account for ratio to D* pi
 0.000044       MyD_2*-         MyD_s+          STS;
 0.0008         MyD_2*-         MyD_s*+         PHSP;
 0.000084       MyD_2*-         MyD_s*(2317)+   STS;
 0.0005         MyD_2*-         MyD_s*(2457)+   PHSP;
 0.000016       MyD_2*-         MyD_s*(2536)+   PHSP;
Enddecay
CDecay anti-B0sig
#
Decay MyD_s+
 0.02           Mya1+           eta             SVS;
 0.02           Mya1+           eta'            SVS;
 0.016          Mya1+           omega           SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.012          Mya1+           phi             SVV_HELAMP 1.0 0.0 1.0 0.0 1.0 0.0;
 0.006          Mya1+           K0              SVS;
 0.007          Mya1+           Myf0            SVS;
 0.001          pi+  pi-  pi+  pi-  pi+         PHSP;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
 0.942          MyD_s+  gamma                   VSP_PWAVE;
 0.058          MyD_s+  pi0                     PHSP;
Enddecay
CDecay MyD_s*-
#
Decay MyD_s*(2317)+
 1.000          MyD_s+  pi0                     PHSP;
Enddecay
CDecay MyD_s*(2317)-
#
Decay MyD_s*(2457)+
 0.18           MyD_s+          gamma           VSP_PWAVE;
 0.48           MyD_s*+         pi0             PHSP;
 0.043          MyD_s+          pi+     pi-     PHSP;
 0.022          MyD_s+          pi0     pi0     PHSP;
 0.04           MyD_s*(2317)+   gamma           VSP_PWAVE;
Enddecay
CDecay MyD_s*(2457)-
#
Decay MyD_s*(2536)+
 0.25           MyD_s+          pi+     pi-     PHSP;
 0.125          MyD_s+          pi0     pi0     PHSP;
 0.1            MyD_s*+         gamma           PHSP;
Enddecay
CDecay MyD_s*(2536)-
#
Decay MyMainD*+
 1.000          MyD0    pi+                     VSS;
Enddecay
CDecay MyMainD*-
#
Decay MyD0
 1.000          K-      pi+                     PHSP;
Enddecay
CDecay anti-MyD0
#
Decay Mya1+
 1.00           rho0    pi+                     VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya1-
#
Decay Myf0
 1.000          pi+     pi-                     PHSP;
Enddecay
#
SetLineshapePW D_1+ D*+ pi0 2
SetLineshapePW D_1+ D*0 pi+ 2
SetLineshapePW D_1- D*- pi0 2
SetLineshapePW D_1- anti-D*0 pi- 2
#
SetLineshapePW D_2*+ D*+ pi0 2
SetLineshapePW D_2*+ D*0 pi+ 2
SetLineshapePW D_2*- D*- pi0 2
SetLineshapePW D_2*- anti-D*0 pi- 2
#
Decay MyD_1-
 1.000          MyMainD*-       pi0             VVS_PWAVE  0.0 0.0 0.0 0.0 1.0 0.0;
Enddecay
CDecay MyD_1+
#
Decay MyD'_1-
 1.000          MyMainD*-       pi0             VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay MyD'_1+
#
Decay MyD_2*-
 1.000          MyMainD*-       pi0             TVS_PWAVE  0.0 0.0 1.0 0.0 0.0 0.0;
Enddecay
CDecay MyD_2*+
#
End
