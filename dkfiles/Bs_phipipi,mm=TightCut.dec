# EventType: 13104082
#
# Descriptor: [B_s0 -> (phi(1020) -> K+ K-) pi+ pi-]cc
#
# NickName: Bs_phipipi,mm=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = "[^(B_s0 -> (phi(1020) -> ^K+ ^K-) ^pi+ ^pi-)]CC"
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import MeV" ,
#     'inAcc = in_range ( 0.010 , GTHETA , 0.400 )' ,
#     "maxMpipi   = ( ( GMASS ( 'pi+' == GID , 'pi-' == GID ) ) < 2000 * MeV )" ]
# tightCut.Cuts    =    {
#     '[K+]cc'    : "inAcc",
#     '[pi-]cc'   : "inAcc",
#     '[B_s0]cc'   : "maxMpipi" }
# 
# EndInsertPythonCode
#
# Documentation:
# B_s0 decaying into Phi pi+ pi-, Phi forced into K+ K-
# Daughters in acceptance.
# Maximum m(\pi\pi) of 2 GeV
# EndDocumentation
#
# CPUTime: < 1 min
#
# PhysicsWG: BnoC
# Tested: Yes
# Responsible: Markus Roehrken
# Email: markus.roehrken@cern.ch
# Date: 20180515
#
Alias      MyPhi   phi
ChargeConj MyPhi   MyPhi

#
Decay B_s0sig
  1.000        MyPhi    pi+ pi-            PHSP;
Enddecay
CDecay anti-B_s0sig
#
Decay MyPhi
  1.000        K+       K-                  VSS;
Enddecay
#
End

