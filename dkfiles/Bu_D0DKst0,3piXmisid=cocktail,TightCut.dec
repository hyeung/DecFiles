# EventType: 12297412
#
# Descriptor: [B+ -> (D*(2010)+ -> (D0 -> (rho(770)+ -> pi+ pi0) K-) pi+) (K*(892)0 -> K+ pi-) Myanti-D*0_MISID]cc
#
# NickName: Bu_D0DKst0,3piXmisid=cocktail,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# Documentation: Bu to DuDKst and excited states of D mesons. Charms go to any possible 3 charged pion final state.
#                Generator cuts to ensure both charms produce 3pi final state.
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = "[B+ => ^(Charm) ^(Charm) (K*(892)0 ==> ^K+ ^pi-)]CC"
# tightCut.Preambulo += [
#   'from LoKiCore.functions import in_range'  ,
#   'from GaudiKernel.SystemOfUnits import GeV, MeV',
#   'goodcharm   = (GNINTREE(("pi+"==GABSID) & ( GPT > 250 * MeV ) & (GP > 3000 * MeV) & in_range( 0.010 , GTHETA , 0.400 ) & (GNINTREE(("K0" == GABSID), HepMC.ancestors)==0), HepMC.descendants) > 0.5)',]
# tightCut.Cuts  = {
#  '[K+]cc'   : 'in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & (GP > 3000 * MeV) & in_range( 0.010 , GTHETA , 0.400 )',
#  '[pi-]cc'  : 'in_range( 0.010 , GTHETA , 0.400 ) & ( GPT > 250 * MeV ) & (GP > 3000 * MeV) & in_range( 0.010 , GTHETA , 0.400 )',
#  '[D+]cc'           : 'goodcharm',
#  '[D*(2010)+]cc'    : 'goodcharm',
#  '[D0]cc'           : 'goodcharm',
#  '[D*(2007)0]cc'    : 'goodcharm',}
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# CPUTime: <5min
# Responsible: Harris Bernstein
# Email: hcbernst@syr.edu
# Date: 20200501
#
Alias MyD0_MISID D0
Alias Myanti-D0_MISID anti-D0
ChargeConj MyD0_MISID Myanti-D0_MISID
#
Alias Myrho+ rho+
Alias Myrho- rho-
ChargeConj Myrho+ Myrho-
#
Alias MyK*-_f2 K*-
Alias MyK*+_f2 K*+
ChargeConj MyK*-_f2 MyK*+_f2
#
Alias MyK*0_f K*0
Alias Myanti-K*0_f anti-K*0
ChargeConj MyK*0_f Myanti-K*0_f
#
Alias Myeta' eta'
ChargeConj Myeta' Myeta'
#
Alias Myeta eta
ChargeConj Myeta Myeta
#
Alias Myrho0 rho0
ChargeConj Myrho0 Myrho0
#
Alias Myomega omega
ChargeConj Myomega Myomega
#
Alias MyD+_MISID D+
Alias MyD-_MISID D-
ChargeConj MyD+_MISID MyD-_MISID
#
Alias MyD*0_MISID D*0
Alias Myanti-D*0_MISID anti-D*0
ChargeConj MyD*0_MISID Myanti-D*0_MISID
#
Alias MyD*+_MISID D*+
Alias MyD*-_MISID D*-
ChargeConj MyD*+_MISID MyD*-_MISID
#
Decay B+sig
1.0 Myanti-D0_MISID MyD+_MISID MyK*0_f PHSP;
1.0 Myanti-D*0_MISID MyD+_MISID MyK*0_f PHSP;
1.0 Myanti-D0_MISID MyD*+_MISID MyK*0_f PHSP;
1.0 Myanti-D*0_MISID MyD*+_MISID MyK*0_f PHSP;
Enddecay
CDecay B-sig
#
Decay MyK*-_f2
1.0 K- pi0 PHSP;
Enddecay
CDecay MyK*+_f2
#
Decay MyD0_MISID
1.15 K- pi+ pi0 PHSP;
11.3 Myrho+ K-  SVS;
2.31 MyK*-_f2 pi+ SVS;
1.95 Myanti-K*0_f pi0 SVS;
0.66 K0 K- pi+ PHSP;
0.434 K0 K+ pi- PHSP;
0.643 K- pi+ Myeta' PHSP;
Enddecay
CDecay Myanti-D0_MISID
#
Decay Myomega
1.0 pi+ pi- pi0 OMEGA_DALITZ;
Enddecay
#
Decay MyD+_MISID
6.25 K- pi+ pi+ pi0 PHSP;
0.174 K+ K0 pi+ pi- PHSP;
0.238 K0 K- pi+ pi+ PHSP;
Enddecay
CDecay MyD-_MISID
#
Decay MyD*+_MISID
0.677 MyD0_MISID pi+ VSS;
0.307 MyD+_MISID pi0 VSS;
0.016 MyD+_MISID gamma VSP_PWAVE;
Enddecay
CDecay MyD*-_MISID
#
Decay Myeta
0.3941 gamma gamma PHSP;
0.3268 pi0 pi0 pi0 PHSP;
0.2292 pi+ pi- pi0 PHSP;
0.422 pi+ pi- gamma PHSP;
Enddecay
#
Decay Myrho0
1.0 pi+ pi- VSS;
Enddecay
#
Decay MyK*0_f
1.0 K+ pi- VSS;
Enddecay
CDecay Myanti-K*0_f
#
Decay MyD*0_MISID
0.647 MyD0_MISID pi0 VSS;
0.353 MyD0_MISID gamma VSP_PWAVE;
Enddecay
CDecay Myanti-D*0_MISID
#
Decay Myeta'
0.426 pi+ pi- Myeta PHSP;
0.289 Myrho0 gamma SVP_HELAMP 1.0 0.0 1.0 0.0;
0.228 pi0 pi0 Myeta PHSP;
0.262 Myomega gamma SVP_HELAMP 1.0 0.0 1.0 0.0;
0.222 gamma gamma PHSP;
Enddecay
#
Decay Myrho+
1.0 pi+ pi0 VSS;
Enddecay
CDecay Myrho-
#
End
#
