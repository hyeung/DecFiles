# EventType: 12265008
#
# Descriptor: [B+ -> (anti-D0 -> K+ pi-) pi+ pi- pi+]cc
# NickName: Bu_D0pipipi,Kpi-withf2=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation()
# signal = generation.SignalRepeatedHadronization
# signal.addTool(LoKi__GenCutTool, 'TightCut')
# tightCut = signal.TightCut
# tightCut.Decay = '[^( B- ==> ^(D0 => K- pi+) pi- pi+ pi- ) ]CC'
# tightCut.Preambulo += [
#      'from GaudiKernel.SystemOfUnits import MeV'
#      ,"CS  = LoKi.GenChild.Selector"
#     ,"goodD0 = ( (GP>8000*MeV) & (GPT>1000*MeV) & ( (GCHILD(GPT,('K+' == GABSID )) > 1400*MeV) & (GCHILD(GP,('K+' == GABSID )) > 4000*MeV) & in_range ( 0.010 , GCHILD(GTHETA,('K+' == GABSID )) , 0.400 ) ) & ( (GCHILD(GPT,('pi+' == GABSID )) > 200*MeV) & (GCHILD(GP,('pi+' == GABSID )) > 1600*MeV) & in_range ( 0.010 , GCHILD(GTHETA,('pi+' == GABSID )) , 0.400 ) ) )"
#     ,'goodB = ( ( (GCHILD(GPT,CS("[B- ==> (D0 => K- pi+) ^pi- pi+ pi-]CC")) > 200*MeV) & (GCHILD(GP,CS("[B- ==> (D0 => K- pi+) ^pi- pi+ pi-]CC")) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[B- ==> (D0 => K- pi+) ^pi- pi+ pi-]CC")) , 0.400 ) ) & ( (GCHILD(GPT,CS("[B- ==> (D0 => K- pi+) pi- ^pi+ pi-]CC")) > 200*MeV) & (GCHILD(GP,CS("[B- ==> (D0 => K- pi+) pi- ^pi+ pi-]CC")) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[B- ==> (D0 => K- pi+) pi- ^pi+ pi-]CC")) , 0.400 ) ) & ( (GCHILD(GPT,CS("[B- ==> (D0 => K- pi+) pi- pi+ ^pi-]CC")) > 200*MeV) & (GCHILD(GP,CS("[B- ==> (D0 => K- pi+) pi- pi+ ^pi-]CC")) > 1200*MeV) & in_range ( 0.010 , GCHILD(GTHETA,CS("[B- ==> (D0 => K- pi+) pi- pi+ ^pi-]CC")) , 0.400 ) ) )'
# ]
# tightCut.Cuts = {
#      '[D0]cc': 'goodD0'
#     ,'[B-]cc': 'goodB'
#     }
# EndInsertPythonCode
#
# Documentation: B -> (D0 -> K pi) 3pi decay. Daughters in LHCb Acceptance and passing StrippingBu2D0TauNuForB2XTauNuAllLines cuts.
# EndDocumentation
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Antonio Romero Vidal
# Email: antonio.romero@usc.es
# Date: 20200629
# CPUTime: < 1 min
#
Alias      MyD0               D0
Alias      Myanti-D0          anti-D0
ChargeConj MyD0               Myanti-D0
#
Alias      Mya_1-             a_1-
Alias      Mya_1+             a_1+
ChargeConj Mya_1+             Mya_1-
#
Alias      Myrho0             rho0
ChargeConj Myrho0             Myrho0
#
Alias      Myf_2              f_2
ChargeConj Myf_2              Myf_2
#
Alias      MyD_10             D_10
Alias      Myanti-D_10        anti-D_10
ChargeConj MyD_10             Myanti-D_10
#
Alias      MyD*-              D*-
Alias      MyD*+              D*+
ChargeConj MyD*+              MyD*-
#
Decay B+sig
0.66   Mya_1+ Myanti-D0       SVS;
0.08   Myanti-D0 Myrho0 pi+   PHSP;
0.12   Myanti-D0 Myf_2 pi+    PHSP;
0.14   Myanti-D_10 pi+        SVS;
Enddecay
CDecay B-sig
#
Decay Myanti-D0
  1.000   K+     pi-          PHSP;
Enddecay
CDecay MyD0
#
Decay Mya_1+
  1.000   Myrho0 pi+          VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay MyD_10
  0.7     MyD*+  pi-          VVS_PWAVE  1.0 0.0 0.0 0.0 0.0 0.0;
  0.3     MyD0   pi-  pi+     PHSP;
Enddecay
CDecay Myanti-D_10
#
Decay MyD*+
  1.00    MyD0   pi+          VSS;
Enddecay
CDecay MyD*-
#
Decay Myf_2
  1.00    pi+    pi-          TSS ;
Enddecay
#
Decay Myrho0
  1.000   pi+    pi-          VSS;
Enddecay
#
End

