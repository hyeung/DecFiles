# EventType: 12145152
#
# Descriptor: [B+ -> (J/psi(1S) -> mu+ mu-)  (KS0 -> pi+ pi-) pi+]cc
#
# NickName: Bu_JpsiKSpi,mm=XLL,TightCut,KSVtxCut 
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: 3 min
#
# Documentation: B+ decays to Jpsi(-> mu+ mu-) and KS pi+ with the custom XLL generator,  KS0 VTZ < 2.4m
# EndDocumentation
#
# InsertPythonCode:
# from Configurables import EvtGenDecayWithCutTool
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
#
# gen = Generation()
# gen.SignalRepeatedHadronization.DecayTool = "EvtGenDecayWithCutTool"
# ToolSvc().addTool(EvtGenDecayWithCutTool)
# ToolSvc().EvtGenDecayWithCutTool.DecayFile = "$DECFILESROOT/dkfiles/DECAY.DEC"
#
# gen.SignalRepeatedHadronization.setProp('MaxNumberOfRepetitions', 5000)
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' ) 
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay     = '[B+ => (J/psi(1S) => ^mu+ ^mu-) ^(KS0 => ^pi+ ^pi-) ^pi+]CC'
# tightCut.Cuts      =    {
#     '[pi+]cc'        : ' inAcc' , 
#     '[mu+]cc'        : ' inAcc' , 
#     'KS0'            : ' decayBeforeTT'
#     }
#
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import ns, GeV, mrad, millimeter",
#     "inAcc          = in_range ( 0.005 , GTHETA , 0.400 ) " ,
#     "decayBeforeTT  = GVEV & ( GFAEVX ( GVZ , 1.e+10 ) < 2400 * millimeter)"
#     ]
#
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Biplab Dey
# Email: biplab.dey@cern.ch
# Date: 20201119
#
Alias      MyJ/psi    J/psi
Alias      MyKs         K_S0
ChargeConj MyJ/psi    MyJ/psi
ChargeConj MyKs         MyKs
#
Decay B+sig
  1.0000   MyJ/psi MyKs pi+  XLL 0;
Enddecay
CDecay B-sig
#
Decay MyKs
  1.000         pi+       pi-            PHSP;
Enddecay
#
Decay MyJ/psi
  1.0000   mu+   mu-    PHOTOS    XLL;
Enddecay
#
End
#
