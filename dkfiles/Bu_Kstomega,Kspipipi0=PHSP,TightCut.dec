# EventType: 12105502
#
# Descriptor: [B- -> (K*- -> (K_S0 -> pi+ pi-) pi-) (omega -> pi+ pi- pi0)]cc
#
# NickName: Bu_Kstomega,Kspipipi0=PHSP,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay = '^[ B- -> (K*(892)- -> ^(KS0 => ^pi+ ^pi-) ^pi-) (omega(782) => ^pi+ ^pi- ^pi0) ]CC'
# tightCut.Preambulo += [
#     'GVZ = LoKi.GenVertices.PositionZ() ' ,	
#     'from GaudiKernel.SystemOfUnits import millimeter',
#     'from GaudiKernel.SystemOfUnits import MeV',
#     'inAcc = (in_range(0.005, GTHETA, 0.400))',
#     'goodB = ((GP > 55000 * MeV) & (GPT > 5000 * MeV) & (GTIME > 0.135 * millimeter))',
#     'goodOmega = (GP >  4000 * MeV) &  (GPT >  400 * MeV)',
#     'goodDDaugPi  = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 4) > 4.5)',
#     'goodKsDaugPi = (GNINTREE (("pi+" == GABSID) & (GP > 2000 * MeV) & inAcc, 4) > 1.5)',
#     'goodPi0      = ((GPT > 400 * MeV) & inAcc)'
# ]
# tightCut.Cuts  = { 
#     '[B-]cc'       : 'goodB & goodDDaugPi',
#     '[pi+]cc'        : 'inAcc',
#     '[K+]cc'         : 'inAcc',
#     '[KS0]cc'        : 'goodKsDaugPi',
#     '[omega(782)]cc' : 'goodOmega' ,
#     '[pi0]cc'        : 'goodPi0'
# }
#
# EndInsertPythonCode
#
# Documentation: Tight Cuts
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime: < 1min
# Responsible: Jessy Daniel
# Email: jessy.daniel@cern.ch
# Date: 20230323
#
Alias Myomega omega
ChargeConj Myomega Myomega
Alias MyK*- K*-
Alias MyK*+ K*+
ChargeConj MyK*+ MyK*-
Alias MyK_S0 K_S0
ChargeConj MyK_S0 MyK_S0
#
Decay B-sig
1.000  MyK*-  Myomega  PHSP;
Enddecay
CDecay B+sig
#
Decay MyK*-
1.000  MyK_S0  pi-  PHSP;
Enddecay
CDecay MyK*+
#
Decay Myomega
1.000  pi+  pi-  pi0  PHSP;
Enddecay
#
Decay MyK_S0
1.0000  pi+ pi-   PHSP;
Enddecay
#
End

