# EventType: 23203491 
#
# Descriptor: {[D_s+ -> pi+ pi- ( pi0 -> gamma gamma ) pi+]cc, [D_s+ -> ( eta -> pi+ pi- ( pi0 -> gamma gamma ) ) pi+]cc, [D_s+ -> ( omega -> pi+ pi- (pi0 -> gamma gamma)) pi+]cc, [D_s+ -> ( phi -> pi+ pi- (pi0 -> gamma gamma)) pi+]cc}
#
# NickName: Ds+_pipipipi0,gg=Cocktail,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool, 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '[^(D_s+ ==> ^pi+ ^pi- ^( pi0 -> ^gamma ^gamma ) ^pi+)]CC'
#
# tightCut.Cuts = {
# 	'[pi+]cc'	:	'inAcc & piCuts',
# 	'[D_s+]cc'	:	'Dcuts',
#   	'gamma' 	: 	'goodPhoton',  
# 	'pi0'		:	'goodPi0'}
# tightCut.Preambulo += [
#	  'inAcc = in_range(0.005, GTHETA, 0.400) & in_range(1.8, GETA, 5.0)',
#	  'piCuts = ( (GPT>250 * MeV) & ( GP > 2000 * MeV))',
#	  'Dcuts = (GPT> 2000 * MeV)',
#   	  'goodPi0 = (GPT>1000 * MeV)',
#	  'inEcalX = abs( GPX/GPZ ) <4.5/10',
#	  'inEcalY = abs( GPY/GPZ ) <3.5/10',
#	  'inEcalHole = (abs (GPX/GPZ) <0.25/12.5 ) & (abs(GPY/GPZ)<0.25/12.5)',
#	  'goodPhoton = (GPT >100 * MeV) & (GPZ>0) & inEcalX & inEcalY & ~inEcalHole']
# EndInsertPythonCode	
#
# Documentation: Forces a D_s+ to pi+ pi- ( pi0 -> gamma gamma ) pi+ with generator level cuts 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Arnau Brossa
# Email: arnau.brossa@cern.ch
# Date: 20220609
#
Alias       my_eta   eta
ChargeConj  my_eta   my_eta
Alias       my_omega omega
ChargeConj  my_omega my_omega
Alias       my_phi   phi
ChargeConj  my_phi   my_phi
Alias       my_pi0   pi0
ChargeConj  my_pi0   my_pi0
#
Decay  D_s+sig
  0.214     my_eta   pi+                   PHOTOS PHSP ;
  0.030     my_omega pi+                   PHOTOS SVS ;
  0.567     my_phi   pi+                   PHOTOS SVS ;
  0.189     pi+      pi+   pi-   my_pi0    PHOTOS PHSP ;

Enddecay
CDecay D_s-sig
#
Decay  my_eta
  1.000     pi+      pi-    my_pi0  PHOTOS ETA_DALITZ ;
Enddecay
#
Decay  my_omega
  1.000     pi+     pi-     my_pi0     PHOTOS OMEGA_DALITZ;
Enddecay
#
Decay  my_phi
  1.000     pi+     pi-     my_pi0     PHOTOS PHSP;
Enddecay
#
Decay  my_pi0
  1.000     gamma gamma   PHSP ;
Enddecay
#
End
