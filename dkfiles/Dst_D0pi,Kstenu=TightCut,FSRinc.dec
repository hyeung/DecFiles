# EventType: 27583005
#
# Descriptor: [D*(2010)+ -> (D0 -> (K*- -> pi- X0) e+ nu_e) pi+]cc
#
# NickName: Dst_D0pi,Kstenu=TightCut,FSRinc
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[D*(2010)+ => ^(D0 => (K*(892)- => ^pi- X0) ^e+ nu_e) pi+]CC'
# tightCut.Cuts      =    {
#     '[e+]cc'   : ' goodElectron ' ,
#     '[pi-]cc'    : ' goodPion  ' ,
#     '[D0]cc'    : ' goodD     ' ,
#     '[D*(2010)+]cc'   : ' goodDst & goodSlowPion  ', }
# tightCut.Preambulo += [
#     'inAcc     = in_range ( 0.005 , GTHETA , 0.400 ) & in_range( 1.9 , GETA , 5.0 )' ,
#     'goodElectron  = ( GPT > 500  * MeV ) & ( GP > 6 * GeV )     & inAcc   ' ,
#     'goodPion  = ( GPT > 300  * MeV ) & ( GP > 2 * GeV ) & inAcc  ' ,
#     'goodSlowPion  = GCHILDCUT (  ( GPT > 300  * MeV ) & ( GP > 1000  * MeV ) & inAcc , "[D*(2010)+ =>  Charm ^pi+]CC"  )',
#     'goodD     = ( GPT > 250  * MeV ) & ( GP > 3 * GeV )',
#     'goodDst   = ( GPT > 500  * MeV ) & ( GP > 1 * GeV )', ]
#
# EndInsertPythonCode
#
#
# Documentation: Forces the D* decay to D0(K* e nu). K* is forced to decay to pi- K0bar. Tight cuts.
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Lorenzo Capriotti
# Email: lorenzo.capriotti@cern.ch
# Date: 20180926
# CPUTime: < 1 min
#
Alias MyD0 D0
Alias MyantiD0 anti-D0
ChargeConj MyD0 MyantiD0
#
Alias MyK*- K*-
Alias MyK*+ K*+
ChargeConj MyK*- MyK*+
#
Decay D*+sig
  1.000 MyD0  pi+    VSS;
Enddecay
CDecay D*-sig

Decay MyD0
  1.000 MyK*- e+ nu_e PHOTOS ISGW2;  
Enddecay
CDecay MyantiD0
#
Decay MyK*-
  1.00000  pi- anti-K0	   VSS;
Enddecay
CDecay MyK*+
End

