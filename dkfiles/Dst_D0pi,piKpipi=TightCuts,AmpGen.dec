# EventType: 27165072
#
# Descriptor: [D*+ -> (D0 -> K+ pi- pi- pi+) pi+]cc
#
# NickName: Dst_D0pi,piKpipi=TightCuts,AmpGen
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# minPTAndDaughtersInLHCb = gen.SignalPlain.TightCut
# minPTAndDaughtersInLHCb.Decay     = '^[ D*(2010)+ => ^( D0 => ^K+ ^pi- ^pi- ^pi+ ) ^pi+ ]CC'
# minPTAndDaughtersInLHCb.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV ',
#     'inAcc     = in_range ( 0.010 , GTHETA , 0.400 ) ',
#     'goodD       = ( GPT > 2000 * MeV ) & ( GP > 30000 * MeV )',
#     'goodDst     = ( GPT > 2070 * MeV ) & ( GP > 31400 * MeV )'
# ]
# minPTAndDaughtersInLHCb.Cuts      =    {
#     '[pi+]cc'         : 'inAcc',
#     '[K+]cc'          : 'inAcc',
#     '[D0]cc'          : 'goodD',
#     '[D*(2010)+]cc'   : 'goodDst',
#     }
#
# EndInsertPythonCode
#
#
# Documentation:
#   Doubly Cabibbo suppressed D decay.
# EndDocumentation
#
# CPUTime: < 1 min
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Daniel O'Hanlon
# Email: daniel.ohanlon@cern.ch
# Date: 20200601
#
Alias MyD0         D0
Alias Myanti-D0    anti-D0
ChargeConj MyD0    Myanti-D0

Decay D*+sig
  1.000 MyD0 pi+  VSS;
Enddecay
CDecay D*-sig

noPhotos
Decay MyD0
  1.0 K+ pi- pi- pi+ LbAmpGen DtopiKpipi_v2;
Enddecay
CDecay Myanti-D0
#
End
#
