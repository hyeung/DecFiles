# EventType: 15874300
#
# Descriptor: [Lambda_b0 ==> (D_s- => K+ K- pi-) (Lambda_c+ => mu+ nu_mu ...) ...]cc
#
# NickName: Lb_DsstLc,DsmunuX=cocktail,mu3hInAcc
#
# Cuts: BeautyTomuCharmTo3h
#
# Documentation:
# Force Lb to combination of LcDs, LcDs*, and LcDsPi0.
# In each case, Lc->XMuNu and Ds->KKPi.
# For background study of semileptonic Bs->(Ds->KKpi)MuNu decays.
# EndDocumentation
#
# CPUTime: 3 min
#
# PhysicsWG: B2SL
# Tested: Yes
# Responsible: Michael Wilkinson
# Email: miwilkin@syr.edu
# Date: 20201207
#
Alias      MyLambda_c+      Lambda_c+
Alias      Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+      Myanti-Lambda_c-

Alias      MyD_s+           D_s+
Alias      MyD_s-           D_s-
ChargeConj MyD_s+           MyD_s-

Alias      MyD_s*+          D_s*+
Alias      MyD_s*-          D_s*-
ChargeConj MyD_s*+          MyD_s*-

Alias      MyD_s1+    D_s1+
Alias      MyD_s1-    D_s1-
ChargeConj MyD_s1-    MyD_s1+

Alias      MyD'_s1+   D'_s1+
Alias      MyD'_s1-   D'_s1-
ChargeConj MyD'_s1-   MyD'_s1+

Alias      MyD_s0*+   D_s0*+
Alias      MyD_s0*-   D_s0*-
ChargeConj MyD_s0*+   MyD_s0*-
#
Decay Lambda_b0sig
  0.0110000 MyLambda_c+ MyD_s-       PHSP;  # PDG2020
  0.0220000 MyLambda_c+ MyD_s*-      PHSP;
  0.0025000 MyLambda_c+ MyD_s-   pi0 PHSP;
  0.0030181 MyLambda_c+ MyD_s0*-     PHSP;  # ratio to D_s*+ taken from 13874200
  0.0017473 MyLambda_c+ MyD_s1-      PHSP;  # ratio to D_s*+ taken from 13874200
  0.0017473 MyLambda_c+ MyD'_s1-     PHSP;  # ratio to D_s*+ taken from 13874200
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyD_s+
  0.0539 K+ K- pi+ PHOTOS D_DALITZ;
Enddecay
CDecay MyD_s-
#
Decay MyD_s*+
  0.935 MyD_s+ gamma VSP_PWAVE;
  0.058 MyD_s+ pi0   VSS;
Enddecay
CDecay MyD_s*-
#
Decay MyLambda_c+
  0.035 mu+ nu_mu Lambda0     PHOTOS PHSP;
  0.010 mu+ nu_mu Sigma0      PHOTOS PHSP;
  0.010 mu+ nu_mu Sigma*0     PHOTOS PHSP;
  0.006 mu+ nu_mu n0          PHOTOS PHSP;
  0.004 mu+ nu_mu Delta0      PHOTOS PHSP;
  0.012 mu+ nu_mu p+      pi- PHOTOS PHSP;
  0.012 mu+ nu_mu n0      pi0 PHOTOS PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyD_s1+
  0.480 MyD_s*+  pi0       PHOTOS PHSP;
  0.180 MyD_s+   gamma     PHOTOS PHSP;
  0.043 MyD_s+   pi+   pi- PHOTOS PHSP;
  0.080 MyD_s*+  gamma     PHOTOS PHSP;
  0.037 MyD_s0*+ gamma     PHOTOS PHSP;
Enddecay
CDecay MyD_s1-
#
Decay MyD_s0*+
  1.0 MyD_s+ pi0 PHOTOS PHSP;
Enddecay
CDecay MyD_s0*-
#
Decay MyD'_s1+
  0.5 MyD_s*+ gamma     PHOTOS PHSP;
  0.5 MyD_s+  pi+   pi- PHOTOS PHSP;
Enddecay
CDecay MyD'_s1-
#
End
