# EventType: 15144105
#
# Descriptor: [Lambda_b0 -> (J/psi(1S) -> mu+ mu-) (Lambda0 -> p+ pi-)]cc
#
# NickName: Lb_JpsiLambda,mm=phsp,DecProdCut,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
# CPUTime: < 1 min
#
# PolarizedLambdab: yes
#
# Documentation: Lambda0 forced into p pi, includes radiative mode. Decay products in acceptance, and L0(Di)Muon TOS emulation on the Jpsi 
# EndDocumentation
#
# PhysicsWG: Onia
# Tested: Yes
# Responsible: Alessio Piucci
# Email: alessio.piucci@cern.ch
# Date: 20180627
#
# InsertPythonCode:
##
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
##
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[Lambda_b0 ==>  (J/psi(1S) ==> ^mu+ ^mu-) (Lambda0 ==> ^p+ ^pi-)]CC'
##
# tightCut.Cuts = {
#     '[p+]cc'    : ' good_proton ' ,
#     '[pi-]cc'   : ' good_pion ' ,
#     '[mu+]cc'   : ' good_muon ' ,
#     '[J/psi(1S)]cc' : '( l0muon | l0dimuon )'
#    }
##
# tightCut.Preambulo += [
#    "from GaudiKernel.SystemOfUnits import GeV",
#    "inAcc_charged  = in_range ( 0.010 , GTHETA , 0.400 )" ,
#    "inEta          = in_range ( 1.8   , GETA   , 5.1   )" ,
#
#    "good_pion   = ('pi+' == GABSID) & inAcc_charged" ,
#    "good_proton = ('p+' == GABSID) & inAcc_charged" ,
#    "good_muon   = ( 'mu+' == GABSID ) & inAcc_charged &  inEta" ,
#
#    "l0muon    = GINTREE(good_muon &  (GPT > 1.6 * GeV))",
#    "l0dimuon  = (2 == GNINTREE(good_muon)) & (GCHILD(GPT, 'mu+' == GID) * GCHILD(GPT, 'mu-' == GID) > 2.1 * GeV * GeV)"
#    ]
# EndInsertPythonCode

Alias      MyLambda      Lambda0
Alias      Myanti-Lambda anti-Lambda0
ChargeConj Myanti-Lambda MyLambda
Alias      MyJ/psi       J/psi
ChargeConj MyJ/psi       MyJ/psi
#
Decay Lambda_b0sig
  1.000    MyLambda          MyJ/psi                 PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyJ/psi
  1.000     mu+  mu-                      PHOTOS  VLL;
Enddecay
#
Decay MyLambda
  1.000   p+          pi-                      PHSP;
Enddecay
CDecay Myanti-Lambda
#
End
#

