# EventType: 15865013
# Descriptor: [Lambda_b0 ==> (Lambda_c+ -> p+ K- pi+) pi+ pi-  pi- pi+ pi-]cc
#
# NickName: Lb_Sig2455starpi-taunu,Lcpipi,pKpi=cocktail,DecProdCut
#
# Cuts: DaughtersInLHCb
# CutsOptions: NeutralThetaMin 0. NeutralThetaMax 10.
#
# CPUTime: 1min
#
# Documentation: Lb->Lc3pi decay taking into account both resonances for the Lc
# and the 3pi part. Two modes using Lc* resonances are also added.
# This decfile is designed to provide the most suitable production for the
# normalisation channel of Lb->Lctaunu, tau->3pi(pi0)nu_tau.
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Guy Wormser
# Email: wormser@lal.in2p3.fr
# Date: 20180109
#


Alias      MyLambda_c+             Lambda_c+
Alias      Myanti-Lambda_c-        anti-Lambda_c-
ChargeConj MyLambda_c+             Myanti-Lambda_c-

#
Alias      MySigma_c*++             Sigma_c*++
Alias      Myanti-Sigma_c*--        anti-Sigma_c*--
ChargeConj MySigma_c*++             Myanti-Sigma_c*--
#
Alias      MySigma_c*0             Sigma_c*0
Alias      Myanti-Sigma_c*0        anti-Sigma_c*0
ChargeConj MySigma_c*0             Myanti-Sigma_c*0
#
Alias      MyK*0                   K*0
Alias      Myanti-K*0              anti-K*0
ChargeConj MyK*0                   Myanti-K*0
#
Alias      MyLambda(1520)0         Lambda(1520)0
Alias      Myanti-Lambda(1520)0    anti-Lambda(1520)0
ChargeConj MyLambda(1520)0         Myanti-Lambda(1520)0
#
Alias      Mya_1-                  a_1-
Alias      Mya_1+                  a_1+
ChargeConj Mya_1+                  Mya_1-
#
Alias      Myf_2                   f_2
ChargeConj Myf_2                   Myf_2
#
#
Alias      Mytau-                  tau-
Alias      Mytau+                  tau+
ChargeConj Mytau+                  Mytau-
#
Decay Lambda_b0sig
  0.73    MySigma_c*++    pi-        Mytau-   anti-nu_tau     PHSP; 

  0.73    MySigma_c*0     pi+       Mytau-   anti-nu_tau     PHSP; 
  

Enddecay
CDecay anti-Lambda_b0sig
#
Decay MyLambda_c+
# Lc->pKpi:
  0.02800 p+                 K-      pi+   PHSP;
  0.016   p+                 Myanti-K*0    PHSP;
  0.00860 Delta++            K-            PHSP;
  0.00414 MyLambda(1520)0    pi+           PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
#
Decay MySigma_c*++
1. MyLambda_c+ pi+                           PHSP; #[Reconstructed PDG2011]
Enddecay
CDecay Myanti-Sigma_c*--
#
Decay MySigma_c*0
1. MyLambda_c+ pi-                         PHSP; #[Reconstructed PDG2011]
Enddecay
CDecay Myanti-Sigma_c*0

#

#
Decay MyK*0
  0.6667 K+                  pi-           VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Mya_1+
  1.000   rho0               pi+           VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay Myf_2
  1.000  pi+                 pi-           TSS ;
Enddecay
# Tauola steering options
Define TauolaCurrentOption 1
Define TauolaBR1 1.0 
# tau -> pi- pi+ pi- nu_tau
Decay Mytau-
 1. TAUOLA 5;
Enddecay
CDecay Mytau+
#
End
