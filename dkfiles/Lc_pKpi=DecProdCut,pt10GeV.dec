# EventType: 25103063
#
# Descriptor: [Lambda_c+ -> p+ K- pi+]cc
#
# NickName: Lc_pKpi=DecProdCut,pt10GeV
#
# Cuts: LoKi::GenCutTool/MinPTAndDaughtersInLHCb
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'MinPTAndDaughtersInLHCb' )
# minPTAndDaughtersInLHCb = gen.SignalPlain.MinPTAndDaughtersInLHCb
# minPTAndDaughtersInLHCb.Decay     = '^[Lambda_c+ => ^p+ ^K- ^pi+]CC'
# minPTAndDaughtersInLHCb.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import MeV ',
#     'inAcc     = in_range ( 0.010 , GTHETA , 0.400 ) ',
#     'DPT       = ( GPT > 10000 * MeV )'
# ]
# minPTAndDaughtersInLHCb.Cuts      =    {
#     '[pi+]cc'         : 'inAcc',
#     '[K+]cc'          : 'inAcc',
#     '[p+]cc'          : 'inAcc',
#     '[Lambda_c+]cc'   : 'DPT',
#     }
#
# EndInsertPythonCode
#
#
# Documentation:
#   Intended to be a copy of Lc_pKpi=DecProdCut.dec that requires that
#   the Lambda_c+ has PT > 10 GeV.
#   Decay products in acceptance.
# EndDocumentation
#
# CPUTime: 8 min
#
# PhysicsWG: QCD
# Tested: Yes
# Responsible: Daniel Craik
# Email: dcraik@cern.ch
# Date: 20171114
#
Decay Lambda_c+sig
  1.000         p+      K-      pi+     PHSP;
Enddecay
CDecay anti-Lambda_c-sig
#
End
