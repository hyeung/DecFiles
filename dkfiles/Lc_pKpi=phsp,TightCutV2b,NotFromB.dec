# EventType: 25103065
#
# Descriptor: [Lambda_c+ -> p+ K- pi+]cc
#
# NickName: Lc_pKpi=phsp,TightCutV2b,NotFromB
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: (prompt only) Lambda_c decay according to phase space decay model with tight cuts from the Lc2pkpi amplitude analysis LHCb-ANA-2022-029
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '^[Lambda_c+ ==> ^p+ ^K- ^pi+]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV,ns",
#     "GY           =  LoKi.GenParticles.Rapidity () ## to be sure " , 
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         " ,
#     "inEta        =  in_range ( 1.75  , GETA   , 5.15 )          " ,
#     "fastTrack    =  ( GPT > 200 * MeV ) & ( GP  > 1.0 * GeV )   " , 
#     "goodTrack    =  inAcc & inEta & fastTrack                   " ,     
#     "inY          =  in_range ( 1.85   , GY     , 4.85   )       " ,
#     "dauPT        =  GCHILD(GPT,('p+' == GABSID )) + GCHILD(GPT,('K-' == GABSID )) + GCHILD(GPT,('pi+' == GABSID ))", 
#     "goodLc       =  inY & ( dauPT > 3 * GeV ) & ( GPT > 2.5 * GeV ) & ( GP > 20 * GeV ) & (GTIME > 0.00015 * ns)" ,
#     "Bancestors   =  GNINTREE ( GBEAUTY , HepMC.ancestors )       " , 
#     "notFromB     =  0 == Bancestors                              " , 
#
# ]
# tightCut.Cuts     =    {
#     "[Lambda_c+]cc"  : "goodLc & notFromB" ,
#     "[K+]cc"         : "goodTrack & ( GP > 2.9 * GeV ) & ( GPT > 490 * MeV )" , 
#     "[pi+]cc"        : "goodTrack & ( GP > 2.9 * GeV ) & ( GPT > 490 * MeV )" , 
#     "[p+]cc"         : "goodTrack & ( GP > 10 * GeV ) & ( GP < 110 * GeV ) & ( GPT > 0.9 * GeV )"
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: <1min
# Responsible: Elisabeth Maria Niel 
# Email:       elisabeth.maria.niel@cern.ch
# Date: 20220602
#
Decay Lambda_c+sig
  1.000         p+      K-      pi+     PHSP;
Enddecay
CDecay anti-Lambda_c-sig
#
End 
