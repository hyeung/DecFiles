# EventType: 16575131
#
# Descriptor: [Xi_b- -> (Omega_c0 -> (Xi- -> (Lambda0 -> p+ pi-) pi-) mu+ nu_mu) pi-]cc
#
# NickName:  Omegab_Omegacpi,Ximunu=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# ParticleValue: "Xi_b- 122 5132 -1.0 6.0452 1.64e-012 Xi_b- 5132 0.000000e+000", "Xi_b~+ 123 -5132 1.0 6.0452 1.64e-012 anti-Xi_b+ -5132 0.000000e+000"
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# from Configurables import ToolSvc
# gen = Generation()
#
# gen.SignalRepeatedHadronization.addTool(LoKi__GenCutTool ,'TightCut')
# tightCut = gen.SignalRepeatedHadronization.TightCut
# tightCut.Decay   = '[^(Xi_b- ==> ^(Omega_c0 ==> ^(Xi- ==> ^(Lambda0 ==> p+ pi-) pi-) mu+ nu_mu) pi-)]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, centimeter',
#     'inAcc    = in_range ( 1.80 , GETA , 5.10 )' ,
#     'goodLmd = ( GPT > 0.75 * GeV ) & ( GP > 4.95 * GeV ) & (GFAEVX( abs( GVZ  ) , 0 ) < 2500 * mm)',
#     'goodPionFromXim = ( GPT > 0.5 * GeV ) & ( GP > 3.0 * GeV ) & inAcc ' ,
#     'goodMuonFromOmegac = ( GPT > 0.5 * GeV ) & ( GP > 3.0 * GeV ) & inAcc ' ,
#     'goodPionFromOmegab = ( GPT > 0.15 * GeV ) & inAcc ' ,
#     'goodXim = ( GPT > 0.25 * GeV ) & ( GP > 1.0 * GeV ) & (GNINTREE(("pi-"==GABSID) & goodPionFromXim, HepMC.children) > 0)',
#     'goodOmegac = (GNINTREE(("mu+" == GABSID) & goodMuonFromOmegac, HepMC.children) > 0 )',
#     'goodOmegab = (GFAEVX(GVZ, 0) - GFAPVX(GVZ, 0) > 0.8 * millimeter) & (GNINTREE(("pi-"==GABSID) & goodPionFromOmegab, HepMC.children) > 0)'
#     ]
# tightCut.Cuts      =    {
#     '[Xi_b-]cc'  : 'goodOmegab',
#     '[Omega_c0]cc'  : 'goodOmegac',
#     '[Xi-]cc'       : 'goodXim',
#     '[Lambda0]cc'   : 'goodLmd'
#     }
# EndInsertPythonCode
#
# CPUTime: 16min
#
# Documentation: Omegab->Omegacpi decay with Omegac->Xi mu nu
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# Responsible: Ying Liu 
# Email: ying.liu@cern.ch
# Date: 20230103
#
Alias      MyOmega_c0             Omega_c0
Alias      Myanti-Omega_c0        anti-Omega_c0
ChargeConj MyOmega_c0             Myanti-Omega_c0
#
#
Alias      MyLambda0      Lambda0
Alias      MyAntiLambda0  anti-Lambda0
ChargeConj MyLambda0      MyAntiLambda0
#
Alias      MyXim      Xi-
Alias      MyAntiXip  anti-Xi+
ChargeConj MyXim      MyAntiXip
#
Decay Xi_b-sig
  1.0   MyOmega_c0  pi- PHSP;
Enddecay
CDecay anti-Xi_b+sig 
#
Decay MyOmega_c0
  1.0  MyXim      mu+ nu_mu  PHSP;
Enddecay
CDecay Myanti-Omega_c0
#
Decay MyXim
  1.0     MyLambda0   pi-      PHSP;
Enddecay
CDecay MyAntiXip
#
Decay MyLambda0
  1.0   p+          pi-        PHSP;
Enddecay
CDecay MyAntiLambda0
#
End
