# EventType: 32123000 
#
# Descriptor: [ Sigma+ -> e+ e- p+ ]cc
#
# NickName: Sigma+_pee=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
#
# Documentation: Forces a Sigma+ to p e e, tight generator cut
#  * Sigma+ endvertex z in [-1m,0.8m]
#  * Sigma+ endvertex radial cut at 38mm
# EndDocumentation
#
# CPUTime: < 1 min
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# #
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '[^(Sigma+ => ^p+ ^e+ ^e-)]CC'
# tightCut.Preambulo += [
#     "from GaudiKernel.SystemOfUnits import meter, millimeter, GeV" ,
#     "GY           =  LoKi.GenParticles.Rapidity () ## to be sure " , 
#     "inY          =  in_range ( 1.9   , GY     , 4.6   )         " ,
#     "inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         " ,
#     "inEta        =  in_range ( 1.95  , GETA   , 5.050 )         " , 
#     "goodTrack    =  inAcc & inEta" ,
#     "GVX = LoKi.GenVertices.PositionX() " ,
#     "GVY = LoKi.GenVertices.PositionY() " ,
#     "GVZ = LoKi.GenVertices.PositionZ() " ,
#     "vx    = GFAEVX ( GVX, 100 * meter )  " ,    
#     "vy    = GFAEVX ( GVY, 100 * meter )  " ,
#     "rho2  = vx**2 + vy**2 " ,
#     "rhoK  = rho2 < (38 * millimeter )**2 " , 
#     "decay = in_range ( -1 * meter, GFAEVX ( GVZ, 100 * meter ), 0.8 * meter ) ",
# ]
# tightCut.Cuts      =    {
#     "[Sigma+]cc"  : " decay & rhoK",
#    "[e+]cc"         : "goodTrack " ,
#    "[e-]cc"         : "goodTrack " ,  
#    "[p+]cc"         : "goodTrack "
#                         }
# EndInsertPythonCode
#
# PhysicsWG: RD
# Tested: Yes
# Responsible:     Alexandre Brea Rodriguez
# Email: alexandre.brea.rodriguez@cern.ch
# Date:   20180805
#
Decay Sigma+sig
  1.000      p+ e+ e-   PHSP;
Enddecay

CDecay anti-Sigma-sig
#
End

