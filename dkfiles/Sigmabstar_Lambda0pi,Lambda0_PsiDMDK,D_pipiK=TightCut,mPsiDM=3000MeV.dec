# EventType: 16565004
#
# Descriptor: [Sigma_b*- -> (Lambda_b0 -> ( D- -> K+ pi- pi-) K+  H_30 ) pi-]cc
#
# NickName: Sigmabstar_Lambda0pi,Lambda0_PsiDMDK,D_pipiK=TightCut,mPsiDM=3000MeV
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#    Decay a L0 to a K D -> Kpipi and a redefined H_30 for our need, acting the latter as stable Dark Matter candidate. The L0 comes from a Sigma_b- redefined as a Sigma_b*-.
# EndDocumentation
#
# PhysicsWG: Exotica
# Tested: Yes
# CPUTime: 2 min
# Responsible: Saul Lopez
# Email: saul.lopez.solino@cern.ch
# Date: 20211122
#
#
# InsertPythonCode:
# from Configurables import LHCb__ParticlePropertySvc, LoKi__GenCutTool
# LHCb__ParticlePropertySvc().Particles = [
# "H_30       89       36      0.0     3.000000        1.000000e+16    A0                36    0.00",
# "Sigma_b-   114    5112     -1.0     5.83474000      8.776160e-23   Sigma_b-         5112    0.00",
# "Sigma_b~+  115   -5112      1.0     5.83474000      8.776160e-23   anti-Sigma_b+   -5112    0.00"
# ]
# ## Generator level cuts:
# from Configurables import LoKi__GenCutTool
# Generation().SignalRepeatedHadronization.addTool( LoKi__GenCutTool, "TightCut" )
#
# tightCut = Generation().SignalRepeatedHadronization.TightCut
# tightCut.Decay = '[^(Sigma_b- => (Lambda_b0 => ( D- => K+ pi- pi-) K+  H_30 ) pi-)]CC'
# ### - HepMC::IteratorRange::descendants   4
# tightCut.Preambulo += [ "from GaudiKernel.SystemOfUnits import GeV, mrad"
#                          , "inAcc = in_range(1.9, GETA, 5.0)" 
#                          , "isGoodKaon  = ( ( GPT > 0.25*GeV ) & inAcc & ( 'K+' == GABSID ) )"
#                          , "isGoodPi    = ( ( GPT > 0.25*GeV ) & inAcc & ( 'pi+' == GABSID ) )"
#                          , "isGoodD     = ( ( 'D+' == GABSID ) & ( GNINTREE( isGoodKaon, 1 ) > 0 ) & ( GNINTREE( isGoodPi, 1 ) > 1 ) )"
#                          , "isGoodLb    = ( ( 'Lambda_b0' == GABSID ) & ( GNINTREE( isGoodKaon, 1 ) > 0 ) & ( GNINTREE( isGoodD, 1 ) > 0 ) )"
#                          , "isGoodPiSig = ( ( GPT > 0.10*GeV ) & inAcc & ( 'pi+' ==GABSID) )"
#                          , "isGoodSigma = ( ( 'Sigma_b-' == GABSID ) & ( GNINTREE( isGoodLb, 1 ) > 0 ) & ( GNINTREE( isGoodPiSig, 1 ) > 0 ) )"]
# tightCut.Cuts = {
# "[Sigma_b-]cc" : "isGoodSigma"
# }
# EndInsertPythonCode
#
Alias      MyD-      D-
Alias      MyD+      D+
ChargeConj MyD-    MyD+
#
Alias  MyH_30     A0
Alias  Myanti-H_30    A0
ChargeConj MyH_30   Myanti-H_30
#
Alias        MyLambda_b0       Lambda_b0
Alias        Myanti-Lambda_b0  anti-Lambda_b0
ChargeConj   MyLambda_b0       Myanti-Lambda_b0
#
Decay Sigma_b-sig
    1.000  MyLambda_b0    pi-   PHSP;
Enddecay
CDecay anti-Sigma_b+sig
#
Decay MyLambda_b0
    1.000    MyD-     K+   MyH_30    PHSP;
Enddecay
CDecay Myanti-Lambda_b0
#
Decay MyD-
    1.000    K+    pi-   pi-          D_DALITZ;
Enddecay
CDecay MyD+
#
End
