# EventType: 28142254
#
# Descriptor: chi_c2 -> (psi(2S) -> mu+ mu-) gamma
#
# ParticleValue: "chi_c2(1P) 765 445 0.0 3.87169 -3.17e-4 chi_c2 445 0.001"
#
# NickName: X3872_psi2Sgamma,mm=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation: X_1(3872) decays to psi(2S)(to mu+ mu-) and gamma with phase space model
# EndDocumentation
# 
# InsertPythonCode:
#
# from Configurables import LoKi__GenCutTool
#
# generation = Generation()
# signal     = generation.SignalPlain
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut            = signal.TightCut
# tightCut.Decay      = 'Meson ==>  ^(psi(2S) ==> ^mu+ ^mu-) ^gamma'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer, MeV, GeV' ,
#     'inAcc          =  in_range ( 0.005 , GTHETA , 0.400 )                           ' ,
#     'inEta          =  in_range ( 1.95  , GETA   , 5.050 )                           ' ,
#     'inY            =  in_range ( 1.9 , GY , 4.6 )                                   ' ,
#     'lhcbTrack      =  inAcc & inEta                                                 ' ,
#     'recoTrack      =  ( GPT > 250 * MeV ) & ( GP > 2.6 * GeV )                      ' , 
#     'goodpsi2S       =  inY & ( GPT > 2.0 * GeV )                                     ' ,
#     'goodMuon       =  lhcbTrack & recoTrack & ( GP > 5.0 * GeV )                    ' ,
#     'goodGamma      =  ( GPT > 200 * MeV ) & ( GP > 3.0 * GeV)                       ' ]
# tightCut.Cuts       =    {
#     'psi(2S)'     : 'goodpsi2S'  ,
#     '[mu+]cc'       : 'goodMuon'  ,
#     'gamma'         : 'goodGamma' }
#
# # -- modify Pythia8 to only generate from Charmonium processes -- #
# from Configurables import Generation, MinimumBias, Pythia8Production, Inclusive, SignalPlain, SignalRepeatedHadronization, Special
#
# Pythia8TurnOffMinbias  = [ "SoftQCD:all     = off" ]
# Pythia8TurnOffMinbias += [ "Bottomonium:all = off" ]
# Pythia8TurnOffMinbias += [ "Charmonium:all  =  on" ]
#
# gen = Generation()
# gen.addTool( MinimumBias , name = "MinimumBias" )
# gen.MinimumBias.ProductionTool = "Pythia8Production"
# gen.MinimumBias.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.MinimumBias.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Inclusive , name = "Inclusive" )
# gen.Inclusive.ProductionTool = "Pythia8Production"
# gen.Inclusive.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Inclusive.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalPlain , name = "SignalPlain" )
# gen.SignalPlain.ProductionTool = "Pythia8Production"
# gen.SignalPlain.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalPlain.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( SignalRepeatedHadronization , name = "SignalRepeatedHadronization" )
# gen.SignalRepeatedHadronization.ProductionTool = "Pythia8Production"
# gen.SignalRepeatedHadronization.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.SignalRepeatedHadronization.Pythia8Production.Commands += Pythia8TurnOffMinbias
#
# gen.addTool( Special , name = "Special" )
# gen.Special.ProductionTool = "Pythia8Production"
# gen.Special.addTool( Pythia8Production , name = "Pythia8Production" )
# gen.Special.Pythia8Production.Commands += Pythia8TurnOffMinbias
# # -- END  -- #
# EndInsertPythonCode
#
#
# PhysicsWG: Onia 
# Tested: Yes
# Responsible: Liupan An
# Email: liupan.an@cern.ch
# Date: 20240116
# CPUTime: < 1 min
#
#
Alias       Mypsi2S     psi(2S)
#
Decay chi_c2sig
  1.000         gamma   Mypsi2S       PHSP;
Enddecay
#
Decay Mypsi2S
  1.000  mu+      mu-    PHOTOS   VLL; 
Enddecay
#
End
#
