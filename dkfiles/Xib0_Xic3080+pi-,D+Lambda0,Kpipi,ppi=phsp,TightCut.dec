# EventType: 16166141
# 
# Descriptor: [Xi_b0 -> ( Xi_c(2790)+ -> (D+ -> K- pi+ pi+ ) (Lambda0 -> p+ pi-)) pi-]CC
# 
# NickName: Xib0_Xic3080+pi-,D+Lambda0,Kpipi,ppi=phsp,TightCut
#
#

# Cuts: LoKi::GenCutTool/TightCut

# Documentation: Xi_b0 decay to D+ Lambda0 pi- (phase space mode) :Xi_b0 -> ( Xi_c(2790)+ -> (D+ -> K- pi+ pi+ ) (Lambda0 -> p+ pi-)) pi- 
# Since Xi_c(3080)+ is not included in EvtGen, we modify Xi_c(2790)+ to replace it.
# All final state particles are required to be within the tight cut.
# EndDocumentation
#
#
# ParticleValue: "Xi_c(2790)+  1051  104324  1.0  3.0772 2.94305e-24  Xi_c(2790)+  0  0.0", "Xi_c(2790)~-  1052  -104324  -1.0  3.0772 2.94305e-24  anti-Xi_c(2790)-  0  0.0"

# InsertPythonCode:
# 
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalRepeatedHadronization.addTool ( LoKi__GenCutTool , 'TightCut' )
# 
#
#tightCut = gen.SignalRepeatedHadronization.TightCut
#tightCut.Decay     = '[Xi_b0 ==> (Xi_c(2790)+ ==> (D+ => ^K- ^pi+ ^pi+ ) ^(Lambda0 ==> ^p+ ^pi-)) ^pi- ]CC'
# tightCut.Cuts      =    {
#     '[K-]cc'     : ' goodKaon   ' ,
#     '[p+]cc'     : ' goodProton ' ,
#     '[pi+]cc'    : ' goodpi     ',
#    '[Lambda0]cc'     : 'goodL0'
#} 
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import GeV, millimeter',
#    'GVZ = LoKi.GenVertices.PositionZ()',
#     'inAcc    = in_range ( 0.010 , GTHETA , 0.400 ) & in_range ( 1.9 , GETA , 5.1 ) ' ,
#     'goodKaon = ( GPT > 0.15 * GeV ) & (GP> 2.*GeV)  & inAcc ' ,
#     'goodProton = ( GPT > 0.15 * GeV ) & (GP> 2.*GeV)  & inAcc ' ,
#     'goodpi  = ( GPT > 0.15 * GeV ) & ( GP > 2. * GeV ) & inAcc ',
#     "goodL0 = (GFAEVX(abs(GVZ),0) < 2500.0 * millimeter) &(GINTREE ( ( 'pi+' == GABSID ) & ( GP > 1.3 * GeV ) )) & (GINTREE ( ( 'p+' == GABSID ) & ( GP > 1.3 * GeV ) ))"
#]
#
# EndInsertPythonCode


# PhysicsWG:   Onia  
# Tested:      Yes
# Responsible: Tianwen Zhou
# CPUTime:  5 min
# Email:      zhoutw@stu.pku.edu.cn 
# Date:      20210307
#

Alias MyXi_c(3080)+  Xi_c(2790)+
Alias My_anti-Xi_c(3080)-  anti-Xi_c(2790)-
ChargeConj MyXi_c(3080)+ My_anti-Xi_c(3080)-


Alias       MyD+          D+
Alias       MyD-          D-
ChargeConj  MyD+          MyD-        
#
Alias       MyLambda      Lambda0
Alias       MyAntiLambda  anti-Lambda0
ChargeConj  MyLambda  MyAntiLambda
#
Decay       Xi_b0sig
1.000     MyXi_c(3080)+   pi-  PHSP;
Enddecay
CDecay anti-Xi_b0sig
#
Decay MyXi_c(3080)+
 1.000  MyD+  MyLambda   PHSP;
 Enddecay
 CDecay  My_anti-Xi_c(3080)-

#
Decay       MyD+
1.000      K-  pi+   pi+      D_DALITZ;
Enddecay
CDecay MyD- 
#
Decay       MyLambda
1.000      p+  pi-   PHSP;
Enddecay
CDecay MyAntiLambda
#
End
#


