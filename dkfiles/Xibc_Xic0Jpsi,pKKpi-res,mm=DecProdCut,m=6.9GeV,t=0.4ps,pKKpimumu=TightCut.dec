# EventType: 16276063
# 
# Descriptor: [ Xi_bc0 -> (Xi_c0 -> p+ K- K- pi+)  (J/psi -> mu+ mu-) ]cc
#
# NickName: Xibc_Xic0Jpsi,pKKpi-res,mm=DecProdCut,m=6.9GeV,t=0.4ps,pKKpimumu=TightCut
#
# Production: GenXicc
#
# Cuts: LoKi::GenCutToolWithDecay/TightCut
#
# ParticleValue: " Xi_bc0 522 5142 0.0 6.90000000 0.400000e-12 Xi_bc0 5142 0.00000000", " Xi_bc~0 523 -5142 0.0 6.90000000 0.400000e-12 anti-Xi_bc0 -5142 0.00000000"
#
# Documentation: decay file of Xi_bc^0 -> (Xi_c0 -> p K- K- pi+)  (J/psi -> mu+ mu-)
# using dedicated GenXicc package for production, cuts with XiccDaughtersInLHCb, phase space decay model used. 
# Xi_bc decay time changed to 0.4ps to be in range of theoretical predictions. Xi_c0 decay modeled with two sub-decays.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutToolWithDecay
# from Gauss.Configuration import *
# generation = Generation()
# production = generation.Special
# production.addTool ( LoKi__GenCutToolWithDecay , 'TightCut' )
# production.CutTool = 'LoKi::GenCutToolWithDecay/TightCut'
# tightCut   = production.TightCut
# tightCut.SignalPID = production.GenXiccProduction.BaryonState
# tightCut.Decay     = '[Xi_bc0 ==> (Xi_c0 ==> ^p+ ^K- ^K- ^pi+) (J/psi(1S) => ^mu+ ^mu-)]CC'
# tightCut.Cuts      =    {
#     '[K-]cc'   : ' goodKaon ' ,
#     '[p+]cc'   : ' goodProton ' ,
#     '[pi+]cc'  : ' goodpi  ' ,
#     '[mu-]cc'  : ' goodmu  ' }
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import GeV',
#     'inAcc    = in_range ( 0.010 , GTHETA , 0.400 ) ' ,
#     'goodKaon = ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodProton = ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodpi  = ( GP > 1.5 * GeV ) & inAcc ' ,
#     'goodmu  = ( GPT > 0.48 * GeV ) & ( GP > 2.98 * GeV ) & inAcc ' ]
# EndInsertPythonCode
#
# PhysicsWG: Onia
# Tested: Yes
# CPUTime: <1min
# Responsible: Zhuoming Li, S. Blusk
# Email: zli59@syr.edu, sblusk@syr.edu
# Date: 20180920
#

Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
Alias MyJ/psi J/psi
ChargeConj MyJ/psi MyJ/psi

Alias MyXi_c0 Xi_c0
Alias Myanti-Xi_c0 anti-Xi_c0
ChargeConj MyXi_c0 Myanti-Xi_c0
#
Decay Xi_bc0sig
  1.00   MyXi_c0  MyJ/psi                  PHSP;
Enddecay
CDecay anti-Xi_bc0sig
#
Decay MyXi_c0
  0.5   p+  K-     Myanti-K*0                           PHSP;
  0.5   p+   K-  K-  pi+                                 PHSP;
Enddecay
CDecay Myanti-Xi_c0
#
Decay MyK*0
  1.000 K+   pi-                   VSS;
Enddecay
CDecay Myanti-K*0
#
Decay MyJ/psi
 1.0000  mu+        mu-                    PHOTOS VLL ;
Enddecay

End

