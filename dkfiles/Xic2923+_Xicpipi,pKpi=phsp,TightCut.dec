# EventType: 26265073
#
# Descriptor: [ Sigma_c*+ -> ( Xi_c+ -> p+ K- pi+ ) pi+ pi- ]cc
#
# NickName: Xic2923+_Xicpipi,pKpi=phsp,TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# CPUTime: 300 sec
#
# Documentation:  Excited Xi_c+(2923) decay according to phase space decay model with tight cuts.
#                 Mass = 2923.04 MeV and Width = 7.1 MeV (lifetime = 9.27055338e-23)
#                 Sigma_c*+ is used to mimic Xic*+
# EndDocumentation
#   
# ParticleValue: "Sigma_c*+             486        4214   1.0      2.92304000      9.27055338e-23                 Sigma_c*+        4214      0.00000000", "Sigma_c*~-            487       -4214  -1.0      2.92304000      9.27055338e-23            anti-Sigma_c*-       -4214      0.00000000"
#  
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '[Sigma_c*+ ==> ^(Xi_c+ => ^p+ ^K- ^pi+) ^pi+ ^pi- ]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'GY           =  LoKi.GenParticles.Rapidity () ## to be sure ' , 
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )         ' ,
#     'inEta        =  in_range ( 1.95  , GETA   , 5.050 )         ' ,
#     'fastTrack    =  ( GPT > 220 * MeV ) & ( GP  > 3.0 * GeV )   ' , 
#     'fastPionTrack=  ( GPT > 180 * MeV ) & ( GP  > 3.0 * GeV )   ' , 
#     'goodTrack    =  inAcc & inEta                               ' ,     
#     'goodXic       =  ( GPT > 0.9 * GeV )   ' ,
# ]
# tightCut.Cuts     =    {
#     '[Xi_c+]cc'      : 'goodXic' ,
#     '[K-]cc'         : 'goodTrack & fastTrack' , 
#     '[pi+]cc'        : 'goodTrack & fastPionTrack' , 
#     '[p+]cc'         : 'goodTrack & fastTrack & ( GP > 9 * GeV ) '
#     }
#
#
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# Responsible: Ryunosuke ONeil
# Email: r.oneil@cern.ch
# Date: 20201014
#
#
Alias MyXi_c+ Xi_c+
Alias Myanti-Xi_c- anti-Xi_c-
ChargeConj MyXi_c+ Myanti-Xi_c-
#
Alias MyXi_c*0  Xi_c*0
Alias Myanti-Xi_c*0  anti-Xi_c*0
ChargeConj MyXi_c*0 Myanti-Xi_c*0
#
#
Decay Sigma_c*+sig
  0.500    MyXi_c+    pi+    pi-    PHSP;
  0.500    MyXi_c*0   pi+           PHSP;
Enddecay
CDecay anti-Sigma_c*-sig
#
Decay MyXi_c*0
  1.000         MyXi_c+ pi-         PHSP;
Enddecay
CDecay Myanti-Xi_c*0
#
Decay MyXi_c+
  1.000         p+      K-    pi+   PHSP;
Enddecay
CDecay Myanti-Xi_c-
End
