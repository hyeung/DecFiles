from Configurables import Generation
from Gaudi.Configuration import *

Generation().PileUpTool = "FixedLuminosityForRareProcess"

importOptions( "$DECFILESROOT/options/SwitchOffAllPythiaProcesses.py" )

from Configurables import Special, PythiaProduction

Generation().addTool( Special )
Generation().Special.addTool( PythiaProduction )

Generation().Special.PythiaProduction.Commands += [ "pyint2 kfpr 480 1 9920443" ,
                                                    "pysubs msub 480 1"
                                                    ]

from Configurables import Pythia8Production

Generation().Special.addTool( Pythia8Production )
Generation().Special.Pythia8Production.Commands += [
                  'Charmonium:states(3PJ)    = 10441,20443,445,9920443',
                  'Charmonium:O(3PJ)[3P0(1)] = 0.05,0.05,0.05,0.05',
                  'Charmonium:O(3PJ)[3S1(8)] = 0.0031,0.0031,0.0031,0.0031',
                  'Charmonium:gg2ccbar(3PJ)[3PJ(1)]g    = off,off,off,on',
                  'Charmonium:qg2ccbar(3PJ)[3PJ(1)]q    = off,off,off,on',
                  'Charmonium:qqbar2ccbar(3PJ)[3PJ(1)]g = off,off,off,on',
                  'Charmonium:gg2ccbar(3PJ)[3S1(8)]g    = off,off,off,on',
                  'Charmonium:qg2ccbar(3PJ)[3S1(8)]q    = off,off,off,on',
                  'Charmonium:qqbar2ccbar(3PJ)[3S1(8)]g = off,off,off,on'
                                                   ]
