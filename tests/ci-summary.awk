BEGIN{
  cnt = 0
  w_cnt = 0
}

/FAILED/{
  sub("\\.FAILED", "", $1)
  failures[cnt] = $1
  cnt += 1
}

/WARNING/{
  sub("\\.WARNING", "", $1)
  warnings[cnt] = $1
  w_cnt += 1
}

END{
  if(cnt){
    print "Failures:"
    for(i in failures) {
      print "-", failures[i]
    }
    exit(0)
  } else if (w_cnt){ 
    print "Warnings:"
    for(i in warnings) {
      print "-", warnings[i]
    }
    exit(0)
  } else {
    print "everything OK"
  }
}
